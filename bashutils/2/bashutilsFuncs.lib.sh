#!/usr/bin/env bash

# @name bashutilsFuncs.lib.sh
# @brief A library for functions that call bash code utilities
# @description
#    This library requires the functions in common functions library to be sourced first
#    The common functions library version sourced must be the same version as this library

# @description
#    This function accepts up to two parameters from the environment.
#    BASH_LINT_PATHS is a list of space-delimited file paths or bash file globs.
#    If not set, function will attempt to find and lint all shell files in cwd, recursively
#    BASH_FAIL_ON_NO_FILES is a true/false boolean that defaults to true.
#    If set to a true, the function fails if no file paths are found after parsing the file globs.
bashShellcheck() {

  {
    local bash_lint_paths &&
    local bash_lint_fail_on_no_files &&
    local -a bash_lint_files &&
    local search_directory
  } || cmnFail "Failed to declare required local variables."

  { 
    # if BASH_LINT_PATHS not set or empty,
    # attempt to recursively find all shell files in cwd and use that as value for BASH_LINT_PATHS
    search_directory="$PWD" &&
    bash_lint_paths="${BASH_LINT_PATHS:-$(shfmt -f "$search_directory" 2> /dev/null)}" &&
    bash_lint_fail_on_no_files="${BASH_LINT_FAIL_ON_NO_FILES:-true}" # default to true if var is empty
  } || cmnFail "Failed to set values for required local variables."

  {
    hash shellcheck &> /dev/null || cmnFail "Required dep (shellcheck) not installed."
    for file in $bash_lint_paths; do
      if [[ -f "$file" ]]; then
        cmnDebug "Adding file ($file) to array of files to be linted."
        bash_lint_files+=( "$file" ) || cmnFail "Failed to append file ($file) to files array."
      fi
    done &&
      if [[ "$bash_lint_fail_on_no_files" =~ [Tt][Rr][Uu][Ee]|[Yy][Ee][Ss]|[Yy] ]]; then
        cmnDebug "Function will FAIL if no files found to lint."
        [[ "${#bash_lint_files[@]}" -gt "0" ]] || cmnFail "No files to lint passed to shellcheck."
      else
        cmnDebug "Function will WARN if no files found to lint."
        [[ "${#bash_lint_files[@]}" -gt "0" ]] || cmnWarn "No files to lint passed to shellcheck."
        return 0
      fi &&
      for file in "${bash_lint_files[@]}"; do
        cmnDebug "Linting $file"
        shellcheck -x "$file" || cmnFail "File ($file) failed shellcheck linting."
      done
  } || cmnFail "Function executution step failed with unhandled error."

}

