# Bashutils Included Pipeline

## Purpose

This pipeline performs bash code quality related tasks.

## Input Variables

### BASH_LINT_PATHS

Purpose:

- Provides shellcheck with a list of files to lint

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI
- Can be used in conjunction with BASH_LINT_FAIL_ON_NO_FILES to change pipeline job behavior

Requirements:

- Variable must be set (or it assumes the default behavior, which is: lint all the shell things)
- If set, value must be a space-delmited list of valid file paths or globs that resolve to valid file paths

### BASH_LINT_FAIL_ON_NO_FILES

Purpose:

- Lets pipeline know if it should fail if no files found when parsing BASH_LINT_PATHS

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI
- Can be used in conjunction with BASH_LINT_PATHS to change behavior of pipeline job
- If a true-like value is passed, pipeline will fail if no valid files found to lint (default)
- If something other than a true-like value is passed (i.e. false), pipeline will still pass if no files linted

Requirements:

- Variable must be set if a "false" behavior is desired
- If "true" is desired, do not set, or set value to: true, True, Yes, Y, or a similar true-like value