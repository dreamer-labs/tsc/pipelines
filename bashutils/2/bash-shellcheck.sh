#!/usr/bin/env bash

# @name bash-shellcheck.sh
# @brief Lints bash files using the `shellcheck` util
# @description
#    See the following for setting ignore rules and other usage:
#    - https://github.com/koalaman/shellcheck

# @description
#    This is the entrypoint for the `bash-shellcheck` job in the `bashutils` pipeline.
main() {

  {
    source /pipelines/libraries/4/commonFuncs.lib.sh &&
    source /pipelines/bashutils/2/bashutilsFuncs.lib.sh
  } || return 1

  bashShellcheck || return 1

}

main ||
  {
    echo -e "\n[FAIL] Failed with unhandled error." 1>&2
    exit 1
  }
