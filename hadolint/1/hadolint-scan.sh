#!/usr/bin/env bash

hlParseHadolintArgs() {

  if [[ "$hadolint_trusted_registries" != "" ]]; then
    IFS=","
    for registry in ${hadolint_trusted_registries}; do
      cmnInfo "Adding trusted registry: ${registry}"
      hadolint_command_args+=("--trusted-registry" "${registry}")
    done
    unset IFS
  fi &&
    if [[ "$hadolint_failure_threshold" != "" ]]; then
      if [[ "$hadolint_failure_threshold" =~ ^(error|warning|info|style)$ ]]; then
        hadolint_command_args+=("--failure-threshold" "${hadolint_failure_threshold}")
      else
        cmnFail "Passed an invalid failure threshold (${hadolint_failure_threshold}) to hadolint."
        echo "  Accepted options: error, warning, info, style" 1>&2
        return 5
      fi
    fi

}

hlScanImages() {

  hash hadolint &>/dev/null ||
    {
      cmnFail "Required dep (hadolint) not installed."
      return 6
    }
  shopt -s globstar &&
    for file in $hadolint_lint_paths; do
      if [[ -f "$file" ]]; then
        cmnDebug "Adding file ($file) to array of files to be linted."
        hadolint_files+=("$file") || cmnFail "Failed to append file ($file) to files array."
      fi
    done &&
    if [[ "$hadolint_fail_on_no_files" =~ [Tt][Rr][Uu][Ee]|[Yy][Ee][Ss]|[Yy] ]]; then
      cmnDebug "Function will FAIL if no files found to lint."
      [[ "${#hadolint_files[@]}" -gt "0" ]] ||
        {
          cmnFail "No files to lint passed to hadolint."
          return 8
        }
    else
      cmnDebug "Function will WARN if no files found to lint."
      [[ "${#hadolint_files[@]}" -gt "0" ]] ||
        {
          cmnWarn "No files to lint passed to hadolint."
          return 0
        }
    fi &&
    cmnInfo "Linting Dockerfiles with Hadolint." &&
    for file in "${hadolint_files[@]}"; do
      if [[ -f "$file" ]]; then
        cmnInfo "Scanning file (${file})."
        echo -e "\n====== CONTENTS BEGIN ======"
        while IFS= read -r line; do echo "$line"; done <"$file"
        echo -e "====== CONTENTS FINISH ======\n"
        echo -e "\n====== RESULTS BEGIN ======"
        {
          hadolint "${hadolint_command_args[@]}" "${file}" &&
            echo -e "====== RESULTS FINISH ======\n" &&
            cmnInfo "File ($file) passed! :-)"
        } ||
          {
            fail=true
            cmnFail "File ($file) failed! :-("
          }
      fi
    done

  {
    [[ "$fail" == "false" ]] &&
      cmnInfo "All hadolint tests passed! :-)"
  } ||
    {
      cmnFail "One or more hadolint tests failed! :-("
      return 7
    }

}

main() {

  {
    {
      local pipelines_prefix &&
        pipelines_prefix="${DL_HADOLINT_PIPELINES_PREFIX:-/pipelines}" &&
        local missing_libs &&
        missing_libs=0 &&
        source "${pipelines_prefix}/libraries/4/commonFuncs.lib.sh" &&
        for func in cmnInfo cmnFail; do
          hash $func &>/dev/null ||
            {
              echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
              missing_libs=1
            }
        done &&
        { [[ "$missing_libs" == "0" ]] || return 2; }
    } ||
      {
        echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
        return 2
      }
  } &&
    {
      {
        local fail &&
          local hadolint_lint_paths &&
          local hadolint_failure_threshold &&
          local hadolint_trusted_registries &&
          local -a hadolint_command_args &&
          local -a hadolint_files
      } ||
        {
          cmnFail "Variable declaration phase failed."
          return 3
        }
    } &&
    {
      {
        fail=false &&
          hadolint_lint_paths="${DL_HADOLINT_LINT_PATHS:-**/*[Dd]ockerfile* *[Dd]ockerfile*}" &&
          hadolint_failure_threshold="${DL_HADOLINT_FAILURE_THRESHOLD:-error}" &&
          hadolint_trusted_registries="${DL_HADOLINT_TRUSTED_REGISTRIES:-gitlab.com:443,registry.gitlab.com,quay.io,docker.io}" &&
          hadolint_fail_on_no_files="${DL_HADOLINT_FAIL_ON_NO_FILES:-true}"
      } ||
        {
          cmnFail "Variable assignment phase failed."
          return 4
        }
    } &&
    {
      {
        hlParseHadolintArgs &&
          hlScanImages
      } ||
        {
          return_code="$?"
          cmnFail "Function execution phase failed."
          return "$return_code"
        }
    }

}

main ||
  {
    exit_code="$?"
    echo -e "\n[FAIL] $0 | Script failed; exiting ${exit_code}."
    exit "$exit_code"
  }
