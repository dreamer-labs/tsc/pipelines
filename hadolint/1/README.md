# Hadolint Included Pipeline

## Purpose

This pipeline performs dockerfile linting.

## Input Variables

### DL_HADOLINT_LINT_PATHS

Purpose:

- Provides hadolint with a list of files to lint

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI
- Can be used in conjunction with DL_HADOLINT_FAIL_ON_NO_FILES to change pipeline job behavior

Requirements:

- Variable must be set (or it assumes the default behavior, which is: lint anything that has [Dd]ockerfile in the name)
- If set, value must be a space-delmited list of valid file paths or globs that resolve to valid file paths

### DL_HADOLINT_FAIL_ON_NO_FILES

Purpose:

- Lets pipeline know if it should fail if no files found when parsing DL_HADOLINT_LINT_PATHS

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI
- Can be used in conjunction with DL_HADOLINT_LINT_PATHS to change behavior of pipeline job
- If a true-like value is passed, pipeline will fail if no valid files found to lint (default)
- If something other than a true-like value is passed (i.e. false, foo, bar, etc.), pipeline will still pass if no files linted

Requirements:

- Variable must be set to a non-true-like value if a "false" behavior is desired
- If "true" is desired, do not set, or set value to: true, True, Yes, Y, or a similar true-like value

### DL_HADOLINT_TRUSTED_REGISTRIES

Purpose:

- Tells Hadolint which FROM images to allow in the linted Dockerfile

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI
- Must be a string delimited list of registry hostnames (i.e. docker.io,quay.io,registry.gitlab.com )
- Defaults to some common, major registries if nothing is passed

Requirements:

- Variable must be set (or it assumes the default behavior)
- If set, value must be a comma-delmited list of registry hostnames you wish to allow

### DL_HADOLINT_FAILURE_THRESHOLD

Purpose:

- Tells Hadolint which error-level to start failing at, based on default rule set

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI
- Defaults to "error"
- Must be either: error, warning, info, or style

Requirements:

- Variable must be set (or it assumes the default behavior)
- If set, value must be a valid, accepted Hadolint threshold value (minus "ignore" and "none")
