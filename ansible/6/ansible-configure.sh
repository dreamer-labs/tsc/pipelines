#!/usr/bin/env bash

# @description
#    This is the entrypoint for the `ansible_configure_*` jobs in the `gitlab-ci.yml` for the `ansible` pipeline.
main() {

  (
    {
      {
        {
          source /pipelines/libraries/5/commonFuncs.lib.sh &&
            source /pipelines/ansible/6/ansibleFuncs.lib.sh &&
            local missing_libs &&
            missing_libs=0 &&
            for func in cmnFail cmnDebug cmnAssertExists \
              ansConfigureGitCreds ansInstallRequirements \
              ansConfigureInventory cmnGetDeploymentEnvPrefix \
              cmnGetDeploymentEnvSuffix; do
              hash "$func" &>/dev/null ||
                {
                  echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
                  missing_libs=1
                }
            done &&
            { [[ "$missing_libs" == "0" ]] || return 1; }
        } || {
          echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
          return 1
        }
      } &&
        {
          {
            local ansible_requirements_file &&
              local ansible_playbook &&
              local deployment_dir &&
              local deployment_key &&
              local gitlab_username &&
              local gitlab_password &&
              local deployment_secret &&
              local deployment_user &&
              local gitlab_instance &&
              local inventory_config &&
              local inventory_template &&
              local inventory_source &&
              local deployment_prefix &&
              local deployment_suffix &&
              local ansible_host_key_checking &&
              local ansible_args &&
              local -A export_vars_array &&
              local export_vars_file_contents
          } ||
            {
              cmnFail "Variable declaration phase failed."
              return 2
            }
        } &&
        {
          {
            ansible_requirements_file="requirements.yml" &&
              ansible_playbook="$DL_ANSIBLE_PLAYBOOK" &&
              deployment_dir="$DL_ANSIBLE_DEPLOYMENT_DIR" &&
              # This is a throwaway variable used to check existence of $ansible_playbook
              ansible_playbook_path="${deployment_dir}/${ansible_playbook}" &&
              cmnAssertExists ansible_playbook_path &&
              cmnDebug "${ansible_playbook_path} exists." &&
              deployment_key="$DL_ANSIBLE_DEPLOYMENT_KEY" &&
              gitlab_username="$DL_ANSIBLE_RO_GITLAB_USERNAME" &&
              gitlab_password="$DL_ANSIBLE_RO_GITLAB_PASSWORD" &&
              deployment_secret="$DL_ANSIBLE_DEPLOYMENT_SECRET" &&
              deployment_user="$DL_ANSIBLE_DEPLOYMENT_USER" &&
              gitlab_instance="$DL_ANSIBLE_GITLAB_INSTANCE" &&
              # defaults to "aws_ec2.yml" because this is the default file the Ansible AWS inventory source plugin looks for
              inventory_config="${DL_ANSIBLE_INVENTORY_CONFIG:-aws_ec2.yml}" &&
              # defaults to "inv-template-aws-ec2.yml" only because this is historically where we have keep AWS templates
              inventory_template="${DL_ANSIBLE_INVENTORY_TEMPLATE:-inv-template-aws-ec2.yml}" &&
              # defaults to the contents of $inventory_config, unless user passes something different
              # useful for mixing local and dynamic inventories since most dynamic plugins have their config file location hardcoded
              inventory_source="${DL_ANSIBLE_INVENTORY_SOURCE:-$inventory_config}" &&
              deployment_prefix="$(cmnGetDeploymentEnvPrefix)" &&
              deployment_suffix="$(cmnGetDeploymentEnvSuffix)" &&
              export_vars_file="${DL_ANSIBLE_EXPORT_VARS_FILE:-/tmp/ansible-exported-vars.sh}" &&
              ansible_host_key_checking="${DL_ANSIBLE_HOST_KEY_CHECKING:-false}" &&
              ansible_args=() &&
              export_vars_array=()
          } ||
            {
              cmnFail "Variable assignment phase failed."
              return 3
            }
        } &&
        {
          {
            {
              cmnInfo "Exporting variables to export vars file ($export_vars_file) used to build inventory file." &&
                export_vars_array[ANSIBLE_REQUIREMENTS_FILE]="$ansible_requirements_file" &&
                export_vars_array[ANSIBLE_PLAYBOOK]="$ansible_playbook" &&
                export_vars_array[DEPLOYMENT_DIR]="$deployment_dir" &&
                export_vars_array[ANSIBLE_PLAYBOOK_PATH]="${deployment_dir}/${ansible_playbook}" &&
                export_vars_array[GITLAB_USERNAME]="$gitlab_username" &&
                export_vars_array[DEPLOYMENT_USER]="$deployment_user" &&
                export_vars_array[GITLAB_INSTANCE]="$gitlab_instance" &&
                export_vars_array[INVENTORY_CONFIG]="$inventory_config" &&
                export_vars_array[INVENTORY_TEMPLATE]="$inventory_template" &&
                export_vars_array[INVENTORY_SOURCE]="$inventory_source" &&
                export_vars_array[DEPLOYMENT_PREFIX]="$deployment_prefix" &&
                export_vars_array[DEPLOYMENT_SUFFIX]="$deployment_suffix" &&
                for key in "${!export_vars_array[@]}"; do
                  echo "export ${key}=\"${export_vars_array[$key]}\"" >>"$export_vars_file" || return 4
                done &&
                for secret in \
                  DEPLOYMENT_KEY \
                  GITLAB_PASSWORD \
                  DEPLOYMENT_SECRET; do
                  echo "# ${secret}=\"???\"   # Not exported for security" >>"$export_vars_file" || return 4
                done &&
                export_vars_file_contents="$(<"${export_vars_file}")" &&
                cmnDebug "${export_vars_file_contents}"
            } ||
              {
                cmnFail "Failed to properly create the exported variables file ($export_vars_file)."
                return 4
              }
          } &&
            {
              hash ansible-playbook &>/dev/null ||
                {
                  cmnFail "Required util (ansible-playbook) not found in path."
                  return 5
                }
            } &&
            {
              cd "$deployment_dir" || return 6
            } &&
            { cmnCopyCACertificates || return 7; } &&
            { ansSetCorrectPerms || return 8; } &&
            if [[ -f "$deployment_key" ]]; then
              hash chmod &>/dev/null ||
                {
                  cmnFail "Required dep (chmod) not installed."
                  return 8
                }
              chmod 0600 "$deployment_key" ||
                {
                  cmnFail "Failed to change mode of deployment key ($deployment_key)."
                  return 9
                }
            fi &&
            { ansConfigureGitCreds "$gitlab_username" "$gitlab_password" "$gitlab_instance" || return 10; } &&
            { ansInstallRequirements "$ansible_requirements_file" || return 11; } &&
            {
              {
                if [[ "$inventory_template" != "none" ]]; then
                  ansConfigureInventory "$inventory_template" "$inventory_config" "$export_vars_file" || return 12
                fi &&
                  ansible_args+=("-i" "$inventory_source") &&
                  cmnDebug "Ansible inventory source ($inventory_source) set." &&
                  if [[ -f "$deployment_secret" ]]; then
                    ansible_args+=("--vault-password-file" "$deployment_secret")
                    cmnDebug "Ansible vault password file ($deployment_secret) set."
                  else
                    cmnDebug "No Ansible vault password file (\$deployment_secret) set"
                  fi &&
                  if [[ -n "$deployment_user" ]]; then
                    export ANSIBLE_REMOTE_USER="$deployment_user"
                    cmnDebug "Ansible remote user ($deployment_user) set."
                  else
                    cmnInfo "No Ansible remote user (\$deployment_user) set"
                  fi &&
                  if [[ -n "$deployment_key" ]]; then
                    export ANSIBLE_PRIVATE_KEY_FILE="$deployment_key"
                    cmnDebug "Ansible private key file ($deployment_key) set."
                  else
                    cmnInfo "No Ansible private key file (\$deployment_key) set"
                  fi &&
                  export DEPLOYMENT_PREFIX="$deployment_prefix" &&
                  cmnDebug "Deployment prefix (\$deployment_prefix) set to value ($deployment_suffix)." &&
                  export DEPLOYMENT_SUFFIX="$deployment_suffix" &&
                  cmnDebug "Deployment suffix (\$deployment_suffix) set to value ($deployment_suffix)." &&
                  export ANSIBLE_HOST_KEY_CHECKING="$ansible_host_key_checking" &&
                  cmnDebug "Ansible host key checking (\$ansible_host_key_checking) set to value ($ansible_host_key_checking)."
              } ||
                {
                  cmnFail "Failed to setup Ansible arguments."
                  return 12
                }
            } &&
            cmnInfo "Running ansible-playbook command against playbook ($ansible_playbook) with args:" &&
            echo "${ansible_args[@]}" &&
            ansible-playbook "$ansible_playbook" "${ansible_args[@]}" || return 13
        } ||
        {
          return_code="$?"
          cmnFail "Function execution phase failed."
          return "${return_code}"
        }
    } ||
      {
        subshell_exit_code="$?"
        cmnFail "Exiting ansible-configure entrypoint subshell with non-zero exit code ($subshell_exit_code)."
        exit "${subshell_exit_code}"
      }
  ) ||
    {
      return_code="$?"
      echo -e "\n[FAIL] main | Exiting main function with non-zero return code (${return_code})."
      return "$return_code"
    }
}

main ||
  {
    exit_code="$?"
    echo -e "\n[FAIL] $0 | Entrypoint script encountered a fatal error; exiting ${exit_code}.\n" 1>&2
    exit "$exit_code"
  }
