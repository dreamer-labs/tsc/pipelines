#!/usr/bin/env bash

# @description
#    This is the entrypoint for the `ansible_sanity_checks` job in the `gitlab-ci.yml` for the `ansible` pipeline.
main() {
  (

    {
      {
        source /pipelines/libraries/5/commonFuncs.lib.sh &&
          local missing_libs &&
          missing_libs=0 &&
          for func in cmnFail cmnAssertSet cmnAssertValid; do
            hash $func &>/dev/null ||
              {
                echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
                missing_libs=1
              }
          done &&
          { [[ "$missing_libs" == "0" ]] || return 1; }
      } || {
        echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
        return 1
      }
    } &&
      {
        cmnAssertSet DL_ANSIBLE_DEPLOYMENT_TYPE &&
          cmnAssertSet DL_ANSIBLE_DEPLOYMENT &&
          if [[ ! "$DL_ANSIBLE_DEPLOYMENT_TYPE" =~ configure-system-(inventory|reports) ]]; then
            {
              {
                cmnAssertSet DL_ANSIBLE_DEPLOYMENT_SECRET &&
                  cmnAssertSet DL_ANSIBLE_RO_GITLAB_USERNAME &&
                  cmnAssertSet DL_ANSIBLE_RO_GITLAB_PASSWORD &&
                  cmnAssertSet DL_ANSIBLE_DEPLOYMENT_DIR &&
                  cmnAssertSet DL_ANSIBLE_GITLAB_INSTANCE
              } || cmnFail "Failed to assert required variables."
            }
          fi
      } || return 1
  )

}

main ||
  {
    echo -e "\n[FAIL] $0 | Entrypoint script encountered a fatal error.\n" 1>&2
    exit 1
  }
