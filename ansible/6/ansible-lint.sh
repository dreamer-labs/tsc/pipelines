#!/usr/bin/env bash

# @description
#    This is the entrypoint for the `ansible_lint` job in the `gitlab.ci.yml` for the `ansible` pipeline.
main() {

  {
    {
      local ansible_pipeline_dir &&
        ansible_pipeline_dir="/pipelines/ansible/6" &&
        source /pipelines/libraries/5/commonFuncs.lib.sh &&
        source ${ansible_pipeline_dir}/ansibleFuncs.lib.sh &&
        local missing_libs &&
        missing_libs=0 &&
        for func in cmnFail cmnDebug ansConfigureGitCreds ansInstallRequirements ansSetCorrectPerms; do
          hash $func &>/dev/null ||
            {
              echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
              missing_libs=1
            }
        done &&
        { [[ "$missing_libs" == "0" ]] || return 2; }
    } ||
      {
        echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
        return 2
      }
  } &&
    {
      {
        local deployment_dir &&
          local ansible_requirements_file &&
          local gitlab_username &&
          local gitlab_password &&
          local gitlab_instance &&
          local linting_failed &&
          local ansible_lint_fail_on_no_files
      } ||
        {
          cmnFail "Variable declaration phase failed."
          return 3
        }
    } &&
    {
      {
        deployment_dir="$DL_ANSIBLE_DEPLOYMENT_DIR" &&
          ansible_requirements_file="requirements.yml" &&
          gitlab_username="$DL_ANSIBLE_RO_GITLAB_USERNAME" &&
          gitlab_password="$DL_ANSIBLE_RO_GITLAB_PASSWORD" &&
          gitlab_instance="$DL_ANSIBLE_GITLAB_INSTANCE" &&
          linting_failed=0 &&
          ansible_lint_fail_on_no_files="${DL_ANSIBLE_LINT_FAIL_ON_NO_FILES:-true}"
      } ||
        {
          cmnFail "Variable assignment phase failed."
          return 4
        }
    } &&
    {
      {
        {
          for util in ansible-lint cd cp; do
            hash "$util" &>/dev/null || {
              cmnFail "Required util ($util) not found in path."
              return 5
            }
          done
        } &&
          cd "$deployment_dir" &&
          cmnCopyCACertificates &&
          ansSetCorrectPerms &&
          ansConfigureGitCreds "$gitlab_username" "$gitlab_password" "$gitlab_instance" &&
          ansInstallRequirements "$ansible_requirements_file" &&
          {
            { [[ -f ".yamllint" ]] && cmnDebug "Using yamllint config provided by user."; } ||
              {
                cmnDebug "Using non-user-provided yamllint config as a default/fallback." &&
                  cp "${ansible_pipeline_dir}/.yamllint" "./.yamllint"
              } ||
              {
                cmnFail "No user-provided .yamllint found; failed to fallback to a default .yamllint."
                return 6
              }
          } &&
          {
            { [[ -f ".ansible-lint" ]] && cmnDebug "Using ansible-lint config provided by user."; } ||
              {
                cmnDebug "Using non-user-provided ansible-lint config as a default/fallback." &&
                  cp "${ansible_pipeline_dir}/.ansible-lint" "./.ansible-lint"
              } ||
              {
                cmnFail "No user-provided .ansible-lint found; failed to fallback to a default .ansible-lint."
                return 7
              }
          } &&
          for file in playbooks/*.yml; do
            if [[ -f "$file" ]]; then
              cmnDebug "Adding file ($file) to array of files to be linted."
              ansible_lint_files+=("$file") ||
                {
                  cmnFail "Failed to append file ($file) to files array."
                  return 8
                }
            fi
          done &&
          if [[ "$ansible_lint_fail_on_no_files" =~ [Tt][Rr][Uu][Ee]|[Yy][Ee][Ss]|[Yy] ]]; then
            cmnDebug "Function will FAIL if no files found to lint."
            [[ "${#ansible_lint_files[@]}" -gt "0" ]] ||
              {
                cmnFail "No files to lint passed to ansible-lint."
                return 9
              }
          else
            cmnDebug "Function will WARN if no files found to lint."
            [[ "${#ansible_lint_files[@]}" -gt "0" ]] ||
              {
                cmnWarn "No files to lint passed to ansible-lint."
                return 0
              }
          fi &&
          for file in "${ansible_lint_files[@]}"; do
            cmnDebug "Linting ansible playbook ($file)."
            ansible-lint "$file" ||
              {
                cmnWarn "Ansible linting failed on playbook ($file)."
                linting_failed=1
              }
          done &&
          {
            [[ "$linting_failed" == "0" ]] ||
              {
                cmnFail "One or more playbooks failed linting."
                return 10
              }
          }
      } ||
        {
          return_code="$?"
          cmnFail "Function execution phase failed."
          return "$return_code"
        }
    }

}

main ||
  {
    exit_code="$?"
    echo -e "\n[FAIL] $0 | Entrypoint script encountered a fatal error.\n" 1>&2
    exit "$exit_code"
  }
