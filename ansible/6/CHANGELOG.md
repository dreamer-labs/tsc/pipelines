# Ansible v5 to v6

## Breaking Changes

- Pipeline and scripts use new variable namespacing (`DL_ANSIBLE_*`)
- Pipeline uses new namespaced stage names (`dl-ansible-*`)
- Pipeline uses new namespaced job and template names (`dl_ansible_*` and `.dl_ansible_*`)
- Ansible now at v7.1.X
- Ansible-lint, Molecule, and other supporting packages all brought up to newer versions to support new Ansible version

## New Features

- Smoke testing of entrypoint scripts introduced
- Code coverage testing of entrypoint scripts introduced
