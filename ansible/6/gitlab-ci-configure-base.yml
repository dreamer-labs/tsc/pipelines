---

# See README.md for this pipeline for full usage instructions

# This pipeline can be called by calling the main "gitlab-ci.yml" in this repo, or called directly.
# When called directly, only sanity checks and linting are performed.
# The other sub-pipelines in this repo (i.e. configure-settings, configure-system) require this pipeline.

# This list should be overridden in downstream pipeline's ".gitlab-ci.yml", but should include all these stages.
# The downstream pipeline maintainer should consolidate all stages from all included pipelines into a single list.
# These are just here for reference purposes only.
stages:
  - dl-sanity-checks-stage
  - dl-lint-stage

variables:
  DL_ANSIBLE_DEPLOYMENT_DIR: ansible

### Templates ###

# This template is shared by all configure_system_* jobs
# This template can be deprecated once our Gitlab instances all support dynamic tags
# See: https://gitlab.com/gitlab-org/gitlab/-/issues/35742
.dl_ansible_configure_base:
  image:
    name: registry.gitlab.com/dreamer-labs/tsc/pipelines/ansible-final:v6-mr48
  variables:
    DL_ANSIBLE_ANSIBLE_FORCE_COLOR: "true"
  script:
    - /usr/bin/env bash /pipelines/ansible/6/ansible-configure.sh
  artifacts:
    when: always
    paths:
      - ansible/playbooks/xccdf-results/*-xccdf_scan_results.xml
    expire_in: never

### Jobs ###

# Used to check for required pipeline variables before the jobs run
# This is done in interest of failing fast and early if required variables are not set
dl_ansible_sanity_checks:
  image: registry.gitlab.com/dreamer-labs/tsc/pipelines/ansible-final:v6-mr48
  stage: dl-sanity-checks-stage
  script:
    - /usr/bin/env bash /pipelines/ansible/6/ansible-sanity-checks.sh
  only:
    refs:
      - branches
    variables:
      - $DL_ANSIBLE_DEPLOYMENT_TYPE =~ /configure-/

# Used to lint ansible playbooks and roles before running them
# This is done in interest of failing fast and early if any of them are invalid and would fail anyway
dl_ansible_lint:
  image: registry.gitlab.com/dreamer-labs/tsc/pipelines/ansible-final:v6-mr48
  stage: dl-sanity-checks-stage
  script:
    - /usr/bin/env bash /pipelines/ansible/6/ansible-lint.sh
  only:
    - merge_requests
  except:
    refs:
      - pushes
  allow_failure: true
