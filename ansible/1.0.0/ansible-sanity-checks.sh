#!/usr/bin/env bash

# @description
#    Performs sanity checks on required environment variables
main() {

  {
    #shellcheck disable=SC1091
    source ../../libraries/1.0.0/commonFuncs.lib.sh
  } || return 1
 
  {
    assert_set DEPLOYMENT_TYPE &&
    assert_set DEPLOYMENT
  } || return 1

}

main ||
{
  echo -e "\n[FAIL] Failed with unhandled error." 1>&2
  exit 1
}
