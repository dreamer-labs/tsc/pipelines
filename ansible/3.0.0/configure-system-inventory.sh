#!/usr/bin/env bash

# @description
#    This is the entrypoint for the `configure_system_inventory` job in the `gitlab.ci.yml` for the `ansible` pipeline.
main() {

  (

    local libs_version &&
      libs_version="2.1.0" &&
      for library in common ansible; do
        source /pipelines/libraries/${libs_version}/${library}Funcs.lib.sh || return 1
      done

    {
      local deployment_dir
    } || cmnFail "Failed to declare required local variables."

    {
      deployment_dir="$DEPLOYMENT_DIR"
    } || cmnFail "Failed to set values for required local variables."

    {
      cd "$deployment_dir" &&
        ansible-inventory -i aws_ec2.yml --graph
    } || cmnFail "Failed with unhandled error."

  ) || exit 1

}

main ||
  {
    echo -e "\n[FAIL] Failed with unhandled error." 1>&2
    exit 1
  }
