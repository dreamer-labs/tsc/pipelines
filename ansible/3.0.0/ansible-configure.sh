#!/usr/bin/env bash

# @description
#    This is the entrypoint for the `ansible_configure_*` jobs in the `gitlab-ci.yml` for the `ansible` pipeline.
main() {

  (

    {
      {
        source /pipelines/libraries/3.0.0/commonFuncs.lib.sh &&
          source /pipelines/ansible/3.0.0/ansibleFuncs.lib.sh &&
          local missing_libs &&
          missing_libs=0 &&
          for func in cmnFail cmnDebug cmnAssertExists ansConfigureGitCreds ansInstallRequirements ansConfigureInventory; do
            hash $func &> /dev/null ||
              {
                echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
                missing_libs=1
              }
          done &&
          { [[ "$missing_libs" == "0" ]] || return 1; }
      } || {
        echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
        return 1
      }
    } &&
      {
        {
          local ansible_requirements_file &&
            local ansible_playbook &&
            local deployment_dir &&
            local deployment_key &&
            local gitlab_username &&
            local gitlab_password &&
            local deployment_secret &&
            local deployment_user &&
            local gitlab_instance
        } || cmnFail "Variable declaration phase failed."
      } &&
      {
        {
          ansible_requirements_file="requirements.yml" &&
            ansible_playbook="$ANSIBLE_PLAYBOOK" &&
            deployment_dir="$DEPLOYMENT_DIR" &&
            # This is a throwaway variable used to check existence of $ansible_playbook
            ansible_playbook_path="${deployment_dir}/${ansible_playbook}" &&
            cmnAssertExists ansible_playbook_path &&
            cmnDebug "${ansible_playbook_path} exists." &&
            deployment_key="$DEPLOYMENT_KEY" &&
            gitlab_username="$RO_GITLAB_USERNAME" &&
            gitlab_password="$RO_GITLAB_PASSWORD" &&
            deployment_secret="$DEPLOYMENT_SECRET" &&
            deployment_user="$DEPLOYMENT_USER" &&
            gitlab_instance="$GITLAB_INSTANCE"
        } || cmnFail "Variable assignment phase failed."
      } &&
      {
        {
          { hash ansible-playbook &> /dev/null || cmnFail "Required util (ansible-playbook) not found in path."; } &&
            cd "$deployment_dir" &&
            chmod 0600 "$deployment_key" &&
            ansConfigureGitCreds "$gitlab_username" "$gitlab_password" "$gitlab_instance" &&
            ansInstallRequirements "$ansible_requirements_file" &&
            ansConfigureInventory &&
            ansible-playbook "$ansible_playbook" \
              -i aws_ec2.yml \
              --vault-password-file "$deployment_secret" \
              --private-key "$deployment_key" \
              --user "$deployment_user" \
              --become
        } || cmnFail "Function execution phase failed."
      }

  ) || return 1

}

main ||
  {
    echo -e "\n[FAIL] $0 | Entrypoint script encountered a fatal error.\n" 1>&2
    exit 1
  }
