#!/usr/bin/env bash

# @description
#    This is the entrypoint for the `configure_system_reports` job in the `gitlab-ci.yml` in the `ansible` pipeline.
main() {

  (

    local libs_version &&
      libs_version="2.1.0" &&
      for library in common ansible; do
        source /pipelines/libraries/${libs_version}/${library}Funcs.lib.sh || return 1
      done

    {
      for file in ansible/playbooks/xccdf-results/*.xml; do
        file_name=$(basename -- "$file")
        results_file="${file_name%.*}"
        oscap xccdf generate report --output "${results_file}.html" "$file"
      done
    } || cmnFail "Failed with unhandled error."

  ) || exit 1

}

main ||
  {
    echo -e "\n[FAIL] Failed with unhandled error." 1>&2
    exit 1
  }
