#!/usr/bin/env bash

# @name ansibleFuncs.lib.sh
# @brief Ansible functions library
# @description
#    A library of common Ansible functions shared by all Ansible-related pipelines

# @description
#    This function accepts two parameters.
#    The first parameter is the username to use for authentication to gitlab.
#    The second parameter is the password to use for authentication to repo1.
#    Configures the Git Credential helper store with a username and password for repo1.
ansConfigureGitCreds() {

  {
    local username &&
      local password &&
      local host
  } || cmnFail "Failed to declare required local variables."

  {
    username="$1" &&
      password="$2" &&
      host="$3"
  } || cmnFail "Failed to set values for required local variables."

  {
    hash git &> /dev/null || cmnFail "Required dep (git) not installed." &&
      git config --global credential.helper store &&
      echo -e "protocol=https\nhost=${host}\nusername=${username}\npassword=${password}\n\n" | git credential approve
  } || cmnFail "Failed to create git credentials entry."

}

# @description
#    Configures the ansible inventroy file by reading a template "inv-template-aws-ec2.yml"
#    and creating an inventory file in "aws_ec2.yml".
ansConfigureInventory() {

  # Parse inventory template and output to aws_ec2.yml for our deployment
  while IFS= read -r line; do
    eval echo "\"$line\"" >> "aws_ec2.yml" # TODO: get rid of this eval if we can
  done < "inv-template-aws-ec2.yml"

}

# @description
#    This function accepts one parameter.
#    The first parameter is a path of an Ansible requirements file.
#    ansible-galaxy will be used to install the requirements listed in this file.
ansInstallRequirements() {

  {
    local requirements_file
  } || cmnFail "Failed to declare required local variables."

  {
    { [[ -f "$1" ]] && requirements_file="$1"; } || cmnFail "Failed to pass valid requirements file."
  } || cmnFail "Failed to set values for required local variables."

  {
    hash ansible-galaxy &> /dev/null || cmnFail "Required dep (ansible-galaxy) not installed."
    ansible-galaxy install -r "${requirements_file}"
  } || cmnFail "Function failed with unhandled error."

}
