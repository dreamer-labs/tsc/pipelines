#!/usr/bin/env bash

# @name ansibleFuncs.lib.sh
# @brief Ansible functions library
# @description
#    A library of common Ansible functions shared by all Ansible-related pipelines

# @description
#    This function ensures that the file permissions are set appropriately for ansible directories and files
#    Ansible will throw linting errors and warnings if they are set too loosely.
ansSetCorrectPerms() {

  local pipeline_debug || cmnFail "Variable declaration phase failed."

  pipeline_debug="$PIPELINE_DEBUG" || cmnFail "Variable assignment phase failed."

  {
    hash chmod &> /dev/null || cmnFail "Required dep (chmod) not installed." &&
      [[ "$pipeline_debug" ]] && chmod -Rv o-rwx,g-w ./ || chmod -R o-rwx,g-w ./
  } || cmnFail "Function execution phase failed."

}

# @description
#    This function accepts two parameters.
#    The first parameter is the username to use for authentication to gitlab.
#    The second parameter is the password to use for authentication to repo1.
#    Configures the Git Credential helper store with a username and password for repo1.
ansConfigureGitCreds() {

  {
    {
      local missing_libs &&
        missing_libs=0 &&
        for func in cmnFail cmnDebug; do
          hash $func &> /dev/null ||
            {
              echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
              missing_libs=1
            }
        done &&
        { [[ "$missing_libs" == "0" ]] || return 1; }
    } || {
      echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
      return 1
    }
  } &&
    {
      {
        local username &&
          local password &&
          local host
      } || cmnFail "Variable declaration phase failed."
    } &&
    {
      {
        username="$1" &&
          password="$2" &&
          host="$3"
      } || cmnFail "Variable assignment phase failed."
    } &&
    {
      {
        hash git &> /dev/null || cmnFail "Required dep (git) not installed." &&
          git config --global credential.helper store &&
          git credential approve < <(echo -e "protocol=https\nhost=${host}\nusername=${username}\npassword=${password}\n\n")
      } || cmnFail "Function execution phase failed."
    }

}

# @description
#    Accepts an Ansible dynamic inventory plugin config template ($1)
#    and creates an Ansible dynamic inventory plugin config file at ($2).
#    Example inventory source plugins:
#      - https://docs.ansible.com/ansible/latest/collections/amazon/aws/aws_ec2_inventory.html
#      - https://docs.ansible.com/ansible/latest/collections/openstack/cloud/openstack_inventory.html
ansConfigureInventory() {

  {
    {
      local missing_libs &&
        missing_libs=0 &&
        for func in cmnFail cmnDebug; do
          hash $func &> /dev/null ||
            {
              echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
              missing_libs=1
            }
        done &&
        { [[ "$missing_libs" == "0" ]] || return 1; }
    } || {
      echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
      return 1
    }
  } &&
    {
      {
        local inventory_template &&
          local inventory_config
      } || cmnFail "Variable declaration phase failed."
    } &&
    {
      {
        { { [[ -f "$1" ]] && inventory_template="$1"; } || cmnFail "Failed to provide valid inventory template file ($1)."; } &&
          inventory_config="$2"
      } || cmnFail "Variable assignment phase failed."
    } &&
    {
      {
        # Parses inventory template and outputs to a dynamic inventory plugin config file for ansible to use
        # envsubst is used to evaluate the bash variables in the template, without using an unsafe bash 'eval'
        { hash envsubst &> /dev/null || cmnFail "Required util (envsubst) not found in path."; } &&
          envsubst < "$inventory_template" > "$inventory_config"
      } || cmnFail "Function execution phase failed."
    }

}

# @description
#    This function accepts one parameter.
#    The first parameter is a path of an Ansible requirements file.
#    ansible-galaxy will be used to install the requirements listed in this file.
ansInstallRequirements() {

  {
    {
      local missing_libs &&
        missing_libs=0 &&
        for func in cmnFail cmnDebug; do
          hash $func &> /dev/null ||
            {
              echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
              missing_libs=1
            }
        done &&
        { [[ "$missing_libs" == "0" ]] || return 1; }
    } || {
      echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
      return 1
    }
  } &&
    {
      {
        local requirements_file
      } || cmnFail "Variable declaration phase failed."
    } &&
    {
      {
        { [[ -f "$1" ]] && requirements_file="$1"; } || cmnFail "Failed to pass valid requirements file."
      } || cmnFail "Variable assignment phase failed."
    } &&
    {
      {
        { hash ansible-galaxy &> /dev/null || cmnFail "Required dep (ansible-galaxy) not installed."; } &&
          ansible-galaxy install -r "${requirements_file}"
      } || cmnFail "Function execution phase failed."
    }

}
