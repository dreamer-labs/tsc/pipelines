#!/usr/bin/env bash

# @description
#    This is the entrypoint for the `configure_system_inventory` job in the `gitlab.ci.yml` for the `ansible` pipeline.
main() {

  (

    {
      {
        source /pipelines/libraries/3.0.0/commonFuncs.lib.sh &&
          source /pipelines/ansible/3.1.0/ansibleFuncs.lib.sh &&
          local missing_libs &&
          missing_libs=0 &&
          for func in cmnFail cmnDebug; do
            hash $func &> /dev/null ||
              {
                echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
                missing_libs=1
              }
          done &&
          { [[ "$missing_libs" == "0" ]] || return 1; }
      } || {
        echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
        return 1
      }
    } &&
      {
        {
          local deployment_dir &&
            local inventory_config
        } || cmnFail "Variable declaration phase failed."
      } &&
      {
        {
          deployment_dir="$DEPLOYMENT_DIR" &&
            inventory_config="${INVENTORY_CONFIG:-aws_ec2.yml}"
        } || cmnFail "Variable assignment phase failed."
      } &&
      {
        {
          cd "$deployment_dir" &&
            ansible-inventory -i "$inventory_config" --graph
        } || cmnFail "Function execution phase failed."
      }

  ) || return 1

}

main ||
  {
    echo -e "\n[FAIL] $0 | Entrypoint script encountered a fatal error.\n" 1>&2
    exit 1
  }
