---

# See README.md for this pipeline for full usage instructions

# This pipeline can be called by calling the main "gitlab-ci.yml" in this repo, or called directly.
# When called directly, only sanity checks, linting, main app configuration, and compliance reports steps are performed.
# When called directly, this pipeline depends on "ansible-ci-configure-base.yml" to be included as well.

# This code can be activated once all our Gitlab instances are at v14.8+
# Until then, downstream repos using this pipeline must add this include to their repo.
# include:  # support for nested repeated includes added in gitlab 14.8
#   - remote: https://gitlab.com/dreamer-labs/tsc/pipelines/-/raw/split-up-ansible/ansible/3.1.0/gitlab-ci-configure-base.yml

# This list should be overridden in downstream pipeline's ".gitlab-ci.yml", but should include all these stages.
# The downstream pipeline maintainer should consolidate all stages from all include pipelines into a single list.
# These are just here for reference purposes only.
stages:
  - sanity
  - lint
  - configure_system
  - configure_system_reports

variables:
  DEPLOYMENT_DIR: ansible

### Templates ###

# This template is shared by all configure_system_compliance_report_* jobs
# This template can be deprecated once our Gitlab instances all support dynamic tags
# See: https://gitlab.com/gitlab-org/gitlab/-/issues/35742
.configure_system_compliance_report:
  stage: configure_system_reports
  image:
    name: registry.gitlab.com/dreamer-labs/tsc/pipelines/openscap-final:mr-11
  script:
    - /usr/bin/env bash /pipelines/ansible/3.1.0/configure-system-reports.sh
  artifacts:
    when: always
    paths:
      - "*.html"
    expire_in: never
  when: always

### Jobs ###

# Used to display the ansible inventory, as configured
# This is used to show which hosts are found by any configured dynamic inventory source plugins/scripts and/or inventory files
configure_system_inventory:
  stage: configure_system
  extends:
    - .configure_base
  script:
    - /usr/bin/env bash /pipelines/ansible/3.1.0/configure-system-inventory.sh
  only:
    refs:
      - branches
    variables:
      - $DEPLOYMENT_TYPE == "configure-system-inventory" && $DEPLOYMENT =~ /^(dev|staging|prod)-.*/
  tags:
    - c1d-dev

# Used to apply day-one application configurations to staging infra via ansible
# The playbook called via this job should contain infrequently run higher-risk configs
configure_system_staging:
  stage: configure_system
  extends:
    - .configure_base
  variables:
    ANSIBLE_PLAYBOOK: playbooks/configure-system.yml
  tags:
    - c1d-staging
  only:
    refs:
      - branches
    variables:
      - $DEPLOYMENT_TYPE == "configure-system" && $DEPLOYMENT =~ /^staging-.*/

# Used to apply day-one application configurations to production infra via ansible
# The playbook called via this job should contain infrequently run higher-risk configs
configure_system_prod:
  stage: configure_system
  extends:
    - .configure_base
  variables:
    ANSIBLE_PLAYBOOK: playbooks/configure-system.yml
  tags:
    - c1d-prod
  only:
    refs:
      - branches
    variables:
      - $DEPLOYMENT_TYPE == "configure-system" && $DEPLOYMENT =~ /^prod-.*/

# Used to generate html reports of security compliance scans on staging infrastructure
# This is run after each run of configure_system_* jobs to update compliance report artifacts
configure_system_compliance_report_staging:
  extends:
    - .configure_system_compliance_report
  only:
    refs:
      - branches
    variables:
      - $DEPLOYMENT_TYPE == "configure-system" && $DEPLOYMENT =~ /^staging-.*/
  needs: ["configure_system_staging"]

# Used to generate html reports of security compliance scans on production infrastructure
# This is run after each run of configure_system_* jobs to update compliance report artifacts
configure_system_compliance_report_prod:
  extends:
    - .configure_system_compliance_report
  only:
    refs:
      - branches
    variables:
      - $DEPLOYMENT_TYPE == "configure-system" && $DEPLOYMENT =~ /^prod-.*/
  needs: ["configure_system_prod"]

...
