#!/usr/bin/env bash

# @description
#    This is the entrypoint for the `configure_system_reports` job in the `gitlab-ci.yml` in the `ansible` pipeline.
main() {

  (

    {
      {
        source /pipelines/libraries/4/commonFuncs.lib.sh &&
          local missing_libs &&
          missing_libs=0 &&
          for func in cmnFail cmnDebug; do
            hash $func &> /dev/null ||
              {
                echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
                missing_libs=1
              }
          done &&
          { [[ "$missing_libs" == "0" ]] || return 1; }
      } || {
        echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
        return 1
      }
    } &&
      {
        {
          local file_name &&
            local results_file
        } || cmnFail "Variable declaration phase failed."
      } &&
      {
        {
          for file in ansible/playbooks/xccdf-results/*.xml; do
            # Removes the last "/" and everything before it, leaving just the file name
            file_name="${file##*/}"
            # Removes the last "." and file extension of the file name
            results_file="${file_name%.*}"
            oscap xccdf generate report --output "${results_file}.html" "$file"
          done
        } || cmnFail "Function execution phase failed."
      }

  ) || return 1

}

main ||
  {
    echo -e "\n[FAIL] $0 | Entrypoint script encountered a fatal error.\n" 1>&2
    exit 1
  }
