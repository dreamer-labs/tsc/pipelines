# Ansible Included Pipeline

## Changes from verson 4

Version 5 is being released to allow for ssh creds and user values to be retrieved from ansible vault or specified without passing them in on the pipeline vars.

- --become is no used specify `become: True` in ansbile
- DEPLOYMENT_KEY is now optional if ansible_ssh_private_key_file is in the playbook vars and vaulted
- DEPLOYMENT_USER is now optional if ansible_user is specified in the playbook

## Purpose

This pipeline performs ansible-related tasks.

It also includes sanity checking on the environment to ensure that required variables were set.

## Deployment Types

For each deployment type/operation described, certain input variables are required to be set, and certain input artifacts are required to be in-place in the downstream infrastructure repository using this pipeline. Those are explained elsewhere in this document.

### Running All Config Settings

Set DEPLOYMENT_TYPE to "configure-system", DEPLOYMENT to the environment you wish to run against, and include "gitlab-ci.yml" for initial deployment.
- This will deploy all high-risk and low-risk configs from the "configure-system" and "configure-settings" pipelines

### Running Only Lower-risk Config Settings

Set DEPLOYMENT_TYPE to "configure-settings", DEPLOYMENT to the environment you wish to run against, and include "gitlab-ci.yml" for only low-risk config changes.
- This is typically only used after initial deployment to make minor changes to ancillary settings
- Do not place anything in the playbook called by this operation that could seriously botch the main application
- The intent of this deployment type is to allow the infra maintainer to call low-risk changes in isolation

### Running Only Higher-risk Config Settings

Set DEPLOYMENT_TYPE to "configure-system", DEPLOYMENT to the environment you wish to run against, and ensure you include only "gitlab-ci-configure-base.yml" and "gitlab-ci-configure-system.yml" if you have no low-risk configuration settings for the service to run.
- This will mean that every change to the system is considered higher-risk since all the initial deploy code will run again
- It is important that configure-system code is idempotent, as a result
- Only use Ansible roles that are well-tested with molecule to ensure idempotency

### Debugging Inventory Issues

Set DEPLOYMENT_TYPE to "configure-system-inventory" and DEPLOYMENT to the environment you wish to run against, and ensure you somehow include "gitlab-ci-configure-system.yml", (either directly, or via "gitlab-ci.yml"), to see what hosts the ansible-playbook will run against when other deployment types are executed.
- This deployment type is only really useful for degbugging inventory-related issues
- This is common when testing/tweaking settings for dynamic inventory source scripts and plugins


### Reporting - XCCDF and Openscap

Reporting is currently focused on STIG/Openscap reporting. However, because openscap is installed in target host systems it requires a separate image to generate html and other reports based on an XCCDF xml report. Report generation requires the openscap-final OCI image contained in this release directory. Keep in mind that the reports are generated from the target OS and passed to the reporting stage as an artifact.

Note: Ubuntu was required for html generation at the time of the release, but that could change if alpine has since released a more recent version of oscap.

## Input Variables

### PIPELINE_DEBUG

Purpose:

- Prints debug messages specified in functions and entrypoint scripts

Usage:

- Toggle on by setting variable to "true"; variable is unset by default

Requirements:

- Optional variable; set to "true" only if you want more verbose messaging to aid in troubleshooting pipelines.

### EXPORT_VARS

Purpose:

- Only valid for ansible-configure.sh (Currently)
- Places vars in an vars export file that can be sourced in a bash script
- Useful if you need access to the vars passed to ansible playbook or other called tooling in the after_script or to export to another job or stage as an artifact

Usage:

- Specify a path to a bash variable export file
- Source that file later to gain access to vars used by the pipeline...

### DEPLOYMENT

Purpose:

- Unique name for a set of infrastructure

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI

Requirements:

- Variable must be set
- Value must have a length greater than zero
- Value must include at least one non-whitespace character

### DEPLOYMENT_TYPE

Purpose:

- Unique name for an operation (or set of operations) performed against a set of infrastructure

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI

Requirements:

- Variable must be set
- Value must have a length greater than zero
- Value must include at least one non-whitespace character

### RO_GITLAB_USERNAME

Purpose:

- Used to log into Gitlab and clone Ansible requirements from private repositories hosed in Repo1

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, Pipeline CI/CD Variables, or the Gitlab web UI.

Requirements:

- Variable must be set
- Value must be a valid Gitlab username

### RO_GITLAB_PASSWORD

Purpose:

- Used to log into Gitlab and clone Ansible requirements from private repositories hosted in Repo1

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, Gitlab CI/CD variables, or the Gitlab web UI.

Requirements:

- Variable must be set
- Value must be a valid Gitlab password

### DEPLOYMENT_DIR

Purpose:

- The deployment directory for ansible. This will default to `ansible/`. This job expects that all ansible playbooks, templates, and variables live in this deployment directory.

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, Gitlab CI/CD variables, or the Gitlab web UI.

Requirements:

- Variable must be set (defaults to `ansible/`)
- Must point to a valid relative path inside of the repository.

### DEPLOYMENT_KEY

Purpose:

- An optional private SSH key which is used to log into remote instances with.

Usage:

- Must be passed in Gitlab CI/CD variables as a file. Not a variable.

Requirements:

- Value must be a valid SSH private key

### DEPLOYMENT_SECRET

Purpose:

- An ansible-vault secret used to unlocked encrypted Ansible variables.

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, Gitlab CI/CD variables, or the Gitlab web UI.

Requirements:

- Variable must be set
- Value must be a valid ansible-vault secret string

### DEPLOYMENT_USER

Purpose:

- A username to use when logging into remote instances via SSH

Usage:

- May be passed in via the GitlabCI pipeline API, a GitlabCI file, Gitlab CI/CD variables, or the Gitlab web UI.

Requirements:

- Value must be a valid username

### ANSIBLE_PLAYBOOK

Purpose:

- The ansible playbook to run

Usage:

- Must be passed in via the GitalbCI pipeline APi, a GitlabCI file, Gitlab CI/CD variables, or the Gitlab web UI.

Requirements:

- Variable must be set
- Variable must point to a file relative to the `$DEPLOYMENT_DIR`
- This job is set by default for the `configure-system` and `configure-setting` jobs. This only needs to be set if you wish to extend these jobs and run additional playbooks.

### ANSIBLE_HOST_KEY_CHECKING

Purpose:

- Changes whether or not ansible runs use host key checking when SSHing into servers

Usage:

- Must be passed in via the GitalbCI pipeline API, a GitlabCI file, Gitlab CI/CD variables, or the Gitlab web UI.
- Accepts either "true" to enable host key checking, or "false" to disable it

Requirements:

- Variable is set to "false" by default, but can be overridden by the pipeline user

### INVENTORY_TEMPLATE

Purpose:

- The filepath to the template used to render an Ansible dynamic inventory source plugin config file to a location specified in INVENTORY_CONFIG

Usage:

- Must be passed in via the GitalbCI pipeline APi, a GitlabCI file, Gitlab CI/CD variables, or the Gitlab web UI.
- The pipeline entrypoint script defaults the value to "inv-template-aws-ec2.yml"; this is the location of the file we have historically used to store this template for AWS infrastructure, which is why it is the default.
- Follow the documentation for the Ansible dynamic inventory source plugin specific to the cloud provider (i.e. AWS, OpenStack, et cetera) when determining how to create the template.
- The template will have its bash variables evaluated before its contents are written to INVENTORY_CONFIG by the pipeline.
- To disable the generation of dynamic inventory source config files, set this variable to "none"

Requirements:

- Variable must be set (or the default value will be assumed)
- Variable must point to a file relative to the `$DEPLOYMENT_DIR`

### INVENTORY_CONFIG

Purpose:

- The destination for the Ansible dynamic inventory source plugin config file that is rendered from the template file specified in the INVENTORY_TEMPLATE variable.

Usage:

- Must be passed in via the GitalbCI pipeline API, a GitlabCI file, Gitlab CI/CD variables, or the Gitlab web UI.
- Defaults to value passed in by entrypoint script, ("aws_ec2.yml"), when not set by the user.

Requirements:

- Variable must be set
- Variable must point to a file location relative to the `$DEPLOYMENT_DIR`
- If string includes a subdirectory, subdirectory must exist or template rendering will fail

### INVENTORY_SOURCE

Purpose:

- The inventory string that is passed to the "-i" flag of the ansible-playbook command in some ansible pipeline jobs.

Usage:

- Must be passed in via the GitalbCI pipeline API, a GitlabCI file, Gitlab CI/CD variables, or the Gitlab web UI.
- Defaults to value passed in by entrypoint script, ("aws_ec2.yml"), when not set by the user.

Requirements:

- Variable must be set
- Variable must point to a file location relative to the `$DEPLOYMENT_DIR`
- Will NOT cause ansible to fail outright if set to an invalid directory or file (but it may not add all hosts to inventory)

### ANSIBLE_LINT_FAIL_ON_NO_FILES

Purpose:

- Lets pipeline know if it should fail if no files found when parsing the glob: "playbooks/*yml"
- This is the location where users of the Ansible pipeline are required to store playbooks relative to $DEPLOYMENT_DIR

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI
- If a true-like value is passed, pipeline will fail if no valid files found to lint (default)
- If something other than a true-like value is passed (i.e. false), pipeline will still pass if no files linted

Requirements:

- Variable must be set if a "false" behavior is desired
- If "true" is desired, do not set, or set value to: true, True, Yes, Y, or a similar true-like value

## Input Artifacts

### configure-system.yml

Purpose:

- The playbook used to configure the system. This should install required software for the application, harden the system, and install the application/system.

Requirements:

- Must exist at "${DEPLOYMENT_DIR}"/playbooks/configure-system.yml

### configure-settings.yml

Purpose:

- The playbook used to configure application settings. Running this playbook should be a low-risk operation. For example, in the monitoring repository this playbook will configure Zabbix dashboards.

Requirements:

- Must exist at "${DEPLOYMENT_DIR}"/playbooks/configure-settings.yml

### inv-template-aws-ec2.yml (or another user-specified template config file)

Purpose:

- The dynamic inventory source plugin config file template to use when running Ansible.
- Generates the actual config file at the location specified in INVENTORY_CONFIG variable.

Requirements:

- Must exist at "${DEPLOYMENT_DIR}"/playbooks/inv-template-aws-ec2.yml
- If an alternate filename is desired, specify with INVENTORY_TEMPLATE variable

### requirements.yml

Purpose:

- Defines the Ansible role and collection requirements for the Ansible run.

Requirements:

- Must exist at "${DEPLOYMENT_DIR}"/playbooks/requirements.yml

### xccdf-results/

Purpose:

- Contains the scan results from the configure-system job. These are xccdf files generated from openSCAP which will be converted to HTML reports in the configure-system-reports jobs.

Requirements:

- Must exist at "${DEPLOYMENT_DIR}"/playbooks/xccdf-results/
- Generated from the configure-system job
