#!/usr/bin/env bash

# @description
#    This is the entrypoint for the `ansible_sanity_checks` job in the `gitlab-ci.yml` for the `ansible` pipeline.
main() {
  (

    {
      {
        source /pipelines/libraries/4/commonFuncs.lib.sh &&
          local missing_libs &&
          missing_libs=0 &&
          for func in cmnFail cmnAssertSet cmnAssertValid; do
            hash $func &> /dev/null ||
              {
                echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
                missing_libs=1
              }
          done &&
          { [[ "$missing_libs" == "0" ]] || return 1; }
      } || {
        echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
        return 1
      }
    } &&
      {
        cmnAssertSet DEPLOYMENT_TYPE &&
          cmnAssertSet DEPLOYMENT &&
          if [[ ! "$DEPLOYMENT_TYPE" =~ configure-system-(inventory|reports) ]]; then
            {
              {
                cmnAssertSet RO_GITLAB_USERNAME &&
                  cmnAssertSet RO_GITLAB_PASSWORD &&
                  cmnAssertSet DEPLOYMENT_DIR &&
                  cmnAssertSet GITLAB_INSTANCE
              } || cmnFail "Failed to assert required variables."
            }
          fi
      } || return 1
  )

}

main ||
  {
    echo -e "\n[FAIL] $0 | Entrypoint script encountered a fatal error.\n" 1>&2
    exit 1
  }
