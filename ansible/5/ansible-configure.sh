#!/usr/bin/env bash

# @description
#    This is the entrypoint for the `ansible_configure_*` jobs in the `gitlab-ci.yml` for the `ansible` pipeline.
main() {

  (

    {
      {
        source /pipelines/libraries/4/commonFuncs.lib.sh &&
          source /pipelines/ansible/5/ansibleFuncs.lib.sh &&
          local missing_libs &&
          missing_libs=0 &&
          for func in cmnFail cmnDebug cmnAssertExists                \
                      ansConfigureGitCreds ansInstallRequirements     \
                      ansConfigureInventory cmnGetDeploymentEnvPrefix \
                      cmnGetDeploymentEnvSuffix; do
            hash $func &> /dev/null ||
              {
                echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
                missing_libs=1
              }
          done &&
          { [[ "$missing_libs" == "0" ]] || return 1; }
      } || {
        echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
        return 1
      }
    } &&
      {
        {
          local ansible_requirements_file &&
            local ansible_playbook &&
            local deployment_dir &&
            local deployment_key &&
            local gitlab_username &&
            local gitlab_password &&
            local deployment_secret &&
            local deployment_user &&
            local gitlab_instance &&
            local inventory_config &&
            local inventory_template &&
            local inventory_source &&
            local deployment_prefix &&
            local deployment_suffix &&
            local ansible_host_key_checking &&
            local ansible_args
        } || cmnFail "Variable declaration phase failed."
      } &&
      {
        {
          ansible_requirements_file="requirements.yml" &&
            ansible_playbook="$ANSIBLE_PLAYBOOK" &&
            deployment_dir="$DEPLOYMENT_DIR" &&
            # This is a throwaway variable used to check existence of $ansible_playbook
            ansible_playbook_path="${deployment_dir}/${ansible_playbook}" &&
            cmnAssertExists ansible_playbook_path &&
            cmnDebug "${ansible_playbook_path} exists." &&
            deployment_key="$DEPLOYMENT_KEY" &&
            gitlab_username="$RO_GITLAB_USERNAME" &&
            gitlab_password="$RO_GITLAB_PASSWORD" &&
            deployment_secret="$DEPLOYMENT_SECRET" &&
            deployment_user="$DEPLOYMENT_USER" &&
            gitlab_instance="$GITLAB_INSTANCE" &&
            # defaults to "aws_ec2.yml" because this is the default file the Ansible AWS inventory source plugin looks for
            inventory_config="${INVENTORY_CONFIG:-aws_ec2.yml}" &&
            # defaults to "inv-template-aws-ec2.yml" only because this is historically where we have keep AWS templates
            inventory_template="${INVENTORY_TEMPLATE:-inv-template-aws-ec2.yml}" &&
            # defaults to the contents of $inventory_config, unless user passes something different
            # useful for mixing local and dynamic inventories since most dynamic plugins have their config file location hardcoded
            inventory_source="${INVENTORY_SOURCE:-$inventory_config}" &&
            deployment_prefix="$(cmnGetDeploymentEnvPrefix)" &&
            deployment_suffix="$(cmnGetDeploymentEnvSuffix)" &&
            export_vars="${EXPORT_VARS:-/tmp/ansible-exported-vars.sh}" &&
            ansible_host_key_checking="${ANSIBLE_HOST_KEY_CHECKING:-false}" &&
            ansible_args=()
        } || cmnFail "Variable assignment phase failed."
      } &&
      {
        echo "[FAIL] ${FUNCNAME[0]} | Exporting variables to $export_vars" &&
          { hash cat &> /dev/null || cmnFail "Missing cat util."; } &&
          cat <<- EXPORTS > "$export_vars"
export ANSIBLE_REQUIREMENTS_FILE="$ansible_requirements_file"
export ANSIBLE_PLAYBOOK="$ansible_playbook"
export DEPLOYMENT_DIR="$deployment_dir"
export ANSIBLE_PLAYBOOK_PATH="${deployment_dir}/${ansible_playbook}"
export DEPLOYMENT_KEY=""  # Not evaluated for security
export GITLAB_USERNAME="$gitlab_username"
# GITLAB_PASSWORD=""      # Not evaluated for security
# DEPLOYMENT_SECRET=""    # Not evaluated for security
export DEPLOYMENT_USER="$deployment_user"
export GITLAB_INSTANCE="$gitlab_instance"
export INVENTORY_CONFIG="$inventory_config"
export INVENTORY_TEMPLATE="$inventory_template"
export INVENTORY_SOURCE="$inventory_source"
export DEPLOYMENT_PREFIX="$deployment_prefix"
export DEPLOYMENT_SUFFIX="$deployment_suffix"
EXPORTS
      } &&
      {
        {
          { hash ansible-playbook &> /dev/null || cmnFail "Required util (ansible-playbook) not found in path."; } &&
            cd "$deployment_dir" &&
            cmnCopyCACertificates &&
            ansSetCorrectPerms &&
            chmod 0600 "$deployment_key" &&
            ansConfigureGitCreds "$gitlab_username" "$gitlab_password" "$gitlab_instance" &&
            ansInstallRequirements "$ansible_requirements_file" &&
            { [[ "$inventory_template" == "none" ]] || ansConfigureInventory "$inventory_template" "$inventory_config" "$export_vars"; } &&
            # If the default file is passed, and it doesn't exist: ansible warns, but still works.
            {
              ansible_args+=("-i" "$inventory_source")
              if [[ -f "$deployment_secret" ]]; then
                ansible_args+=("--vault-password-file" "$deployment_secret")
                cmnDebug "Ansible --vault-password-file: ($deployment_secret)"
              else
                cmnDebug "No vault-password-file set"
              fi
            } &&
            { [[ -n "$deployment_user" ]] && export ANSIBLE_REMOTE_USER="$deployment_user" || cmnInfo "No deployment_user set"; } &&
            { [[ -n "$deployment_key" ]] && export ANSIBLE_PRIVATE_KEY_FILE="$deployment_key" || cmnInfo "No deploy_key set"; } &&
            DEPLOYMENT_PREFIX="$deployment_prefix" \
            DEPLOYMENT_SUFFIX="$deployment_suffix" \
            ANSIBLE_HOST_KEY_CHECKING="$ansible_host_key_checking" \
            ansible-playbook "$ansible_playbook" "${ansible_args[@]}"
        } || cmnFail "Function execution phase failed."
      }

  ) || return 1

}

main ||
  {
    echo -e "\n[FAIL] $0 | Entrypoint script encountered a fatal error.\n" 1>&2
    exit 1
  }
