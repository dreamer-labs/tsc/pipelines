#!/usr/bin/env bash

# @description
#    This is the entrypoint for the `ansible_sanity_checks` job in the `gitlab-ci.yml` for the `ansible` pipeline.
main() {

  local libs_version &&
  libs_version="2.1.0" &&
  source /pipelines/libraries/${libs_version}/commonFuncs.lib.sh || return 1

  {
    cmnAssertSet DEPLOYMENT_TYPE &&
      cmnAssertSet DEPLOYMENT
  } || return 1

  {
    if [[ ! "$DEPLOYMENT_TYPE" =~ configure-system-(inventory|reports) ]]; then
      {
        cmnAssertSet DEPLOYMENT_SECRET &&
          cmnAssertSet DEPLOYMENT_KEY &&
          cmnAssertSet DEPLOYMENT_USER &&
          cmnAssertSet RO_GITLAB_USERNAME &&
          cmnAssertSet RO_GITLAB_PASSWORD &&
          cmnAssertSet DEPLOYMENT_DIR &&
          cmnAssertSet GITLAB_INSTANCE
      } || return 1
    fi
  } || return 1

}


main ||
  {
    echo -e "\n[FAIL] Failed with unhandled error." 1>&2
    exit 1
  }
