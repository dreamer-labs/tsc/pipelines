# Ansible Included Pipeline

## Purpose

This pipeline performs ansible-related tasks.

It also includes sanity checking on the environment to ensure that required variables were set.


## Always Required Input Variables

### DEPLOYMENT

Purpose:

- Unique name for a set of infrastructure

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI

Requirements:

- Variable must be set
- Value must have a length greater than zero
- Value must include at least one non-whitespace character


### DEPLOYMENT_TYPE

Purpose:

- Unique name for an operation (or set of operations) performed against a set of infrastructure

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI

Requirements:

- Variable must be set
- Value must have a length greater than zero
- Value must include at least one non-whitespace character


### RO_GITLAB_USERNAME

Purpose:

- Used to log into Gitlab and clone Ansible requirements from private repositories hosed in Repo1

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, Pipeline CI/CD Variables, or the Gitlab web UI.

Requirements:

- Variable must be set
- Value must be a valid Gitlab username


### RO_GITLAB_PASSWORD

Purpose:

- Used to log into Gitlab and clone Ansible requirements from private repositories hosed in Repo1

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, Gitlab CI/CD variables, or the Gitlab web UI.

Requirements:

- Variable must be set
- Value must be a valid Gitlab password


### DEPLOYMENT_DIR

Purpose:

- The deployment directory for ansible. This will default to `ansible/`. This job expects that all ansible playbooks, templates, and variables live in this deployment directory.

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, Gitlab CI/CD variables, or the Gitlab web UI.

Requirements:

- Variable must be set (defaults to `ansible/`)
- Must point to a valid relative path inside of the repository.


### DEPLOYMENT_KEY

Purpose:

- A private SSH key which is used to log into remote instances with.

Usage:

- Must be passed in Gitlab CI/CD variables as a file. Not a variable.

Requirements:

- Variable must be set
- Value must be a valid SSH private key


### DEPLOYMENT_SECRET

Purpose:

- An ansible-vault secret used to unlocked encrypted Ansible variables.

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, Gitlab CI/CD variables, or the Gitlab web UI.

Requirements:

- Variable must be set
- Value must be a valid ansible-vault secret string


### DEPLOYMENT_USER

Purpose:

- A username to use when logging into remote instances via SSH

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, Gitlab CI/CD variables, or the Gitlab web UI.

Requirements:

- Variable must be set
- Value must be a valid username

# Required Input Artifacts

### configure-system.yml

Purpose:

- The playbook used to configure the system. This should install required software for the application, harden the system, and install the application/system.

Requirements:

- Must exist at "${DEPLOYMENT_DIR}"/playbooks/configure-system.yml

### configure-settings.yml

Purpose:

- The playbook used to configure application settings. Running this playbook should be a low-risk operation. For example, in the monitoring repository this playbook will configure Zabbix dashboards.

Requirements:

- Must exist at "${DEPLOYMENT_DIR}"/playbooks/configure-settings.yml

### inv-template-aws-ec2.yml

Purpose:

- The inventory template file to use when running Ansible.

Requirements:

- Must exist at "${DEPLOYMENT_DIR}"/playbooks/inv-template-aws-ec2.yml

### requirements.yml

Purpose:

- Defines the Ansible role and collection requirements for the Ansible run.

Requirements:

- Must exist at "${DEPLOYMENT_DIR}"/playbooks/requirements.yml

### xccdf-results/

Purpose:

- Contains the scan results from the configure-system job. These are xccdf files generated from openSCAP which will be converted to HTML reports in the configure-system-reports jobs.


Requirements:

- Must exist at "${DEPLOYMENT_DIR}"/playbooks/xccdf-results/
- Generated from the configure-system job
