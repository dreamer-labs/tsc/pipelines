#!/usr/bin/env bash

# @description
#    This is the entrypoint for the `ansible_configure_alerting_*` jobs in the `gitlab-ci.yml` for the `ansible` pipeline.
main() {

  (

    local libs_version &&
      libs_version="2.1.0" &&
      for library in common ansible; do
        source /pipelines/libraries/${libs_version}/${library}Funcs.lib.sh || return 1
      done

    {
      local ansible_requirements_file &&
        local deployment_dir &&
        local deployment_key &&
        local gitlab_username &&
        local gitlab_password &&
        local deployment_secret &&
        local deployment_key &&
        local deployment_user &&
        local gitlab_instance
    } || cmnFail "Failed to declare required local variables."

    {
      ansible_requirements_file="requirements.yml" &&
        deployment_dir="$DEPLOYMENT_DIR" &&
        deployment_key="$DEPLOYMENT_KEY" &&
        gitlab_username="$RO_GITLAB_USERNAME" &&
        gitlab_password="$RO_GITLAB_PASSWORD" &&
        deployment_secret="$DEPLOYMENT_SECRET" &&
        deployment_key="$DEPLOYMENT_KEY" &&
        deployment_user="$DEPLOYMENT_USER" &&
        gitlab_instance="$GITLAB_INSTANCE"
    } || cmnFail "Failed to set values for required local variables."

    {
      cd "$deployment_dir" &&
        chmod 0600 "$deployment_key" &&
        ansConfigureGitCreds "$gitlab_username" "$gitlab_password" "$gitlab_instance" &&
        ansInstallRequirements "$ansible_requirements_file" &&
        ansConfigureInventory &&
        ansible-playbook playbooks/deploy-opsgenie.yml \
          -i aws_ec2.yml \
          --vault-password-file "$deployment_secret" \
          --private-key "$deployment_key" \
          --user "$deployment_user" \
          --become
    } || cmnFail "Failed with unhandled error."

  ) || exit 1

}

main ||
  {
    echo -e "\n[FAIL] Failed with unhandled error." 1>&2
    exit 1
  }
