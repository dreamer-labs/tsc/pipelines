#!/usr/bin/env bash

# @description
#    This is the entrypoint for any jobs that use `.build_images_*` as their template
#    in the `gitlab-ci.yml` for the `buildah` pipeline.

add_custom_ca_certs() {

  # Only needed for logging into docker registries with custom CA certs
  # Gitlab runners can be configured to provide certs to containers via this file path
  if [[ -f "/etc/gitlab-runner/certs/ca.crt" ]]; then
    osAddCACertificate /etc/gitlab-runner/certs/ca.crt ||
      {
        cmnFail "Failed to update-ca-trust for detected gitlab-runner cert."
        return 5
      }
  fi

} 


login_to_primary_registry() {

  {
    cmnInfo "Logging in to primary registry: $ci_registry_user:masked@$ci_registry" &&
      buildah login -u "$ci_registry_user" -p "$ci_registry_password" "$ci_registry" &&
      cmnInfo "Logged in to primary registry: $ci_registry_user:masked@$ci_registry"
  } ||
    {
      cmnFail "Failed to login to primary registry: $ci_registry_user:masked@$ci_registry"
      return 6
    }

}

login_to_extra_registries() {

  if [[ "${ci_extra_registries}" != "" ]]; then
    IFS=","
    for registry in $ci_extra_registries; do
      if [[ "$registry" =~ ^(.+):(.+)@(.+)$ ]]; then
        { 
          cmnInfo "Logging in to extra registry: ${BASH_REMATCH[1]}:masked@${BASH_REMATCH[3]}" &&
            buildah login -u "${BASH_REMATCH[1]}" -p "${BASH_REMATCH[2]}" "${BASH_REMATCH[3]}" &&
            cmnInfo "Logged in to extra registry: ${BASH_REMATCH[1]}:masked@${BASH_REMATCH[3]}"
        } ||
          {
            cmnFail "Failed to login to extra registry: ${BASH_REMATCH[1]}:masked@${BASH_REMATCH[3]}"
            return 7
          }
      fi
    done
    unset IFS
  fi

} 


build_and_push_image() {

  {
    cmnInfo "Building container image (${ci_registry_image}/${target}/${tag})." &&
      buildah build \
        --pull \
        -t "${ci_registry_image}/${target}:${tag}" \
        --target "${target}" \
        --format docker \
        -f "${dockerfile}"\
        "${context}" &&
      buildah push \
        "${ci_registry_image}/${target}:${tag}"
  } ||
    {
      cmnFail "Failed buildah build and push process."
      return 8
    }

}


main() {

  {
    local pipelines_prefix &&
      pipelines_prefix="${DL_BUILDAH_PIPELINES_PREFIX:-/pipelines}" &&
      source "${pipelines_prefix}/libraries/4/commonFuncs.lib.sh" &&
      source "${pipelines_prefix}/libraries/4/osFuncs.lib.sh"
  } ||
    {
      eche -e "\n[FAIL] Failed to source required libraries." 1>&2
      return 1
    }

  {
    local target &&
      local dockerfile &&
      local ci_registry_user &&
      local ci_registry_password &&
      local ci_registry &&
      local ci_extra_registries &&
      local ci_registry_image &&
      local tag &&
      local context
  } ||
    {
      cmnFail "Failed to declare required local variables."
      return 2
    }

  {
    cmnAssertSet CI_REGISTRY_USER &&
      cmnAssertSet CI_REGISTRY_PASSWORD &&
      cmnAssertSet CI_REGISTRY &&
      cmnAssertSet CI_REGISTRY_IMAGE &&
      cmnAssertSet DL_BUILDAH_TARGET &&
      cmnAssertSet DL_BUILDAH_TAG
  } ||
    {
      cmnFail "Failed to assert required environment variables present."
      return 3
    }

  {
    target="$DL_BUILDAH_TARGET" &&
      dockerfile="${DL_BUILDAH_DOCKERFILE:-Dockerfile}" &&
      ci_registry_user="$CI_REGISTRY_USER" &&
      ci_registry_password="$CI_REGISTRY_PASSWORD" &&
      ci_registry="$CI_REGISTRY" &&
      ci_extra_registries="$CI_EXTRA_REGISTRIES" &&
      ci_registry_image="$CI_REGISTRY_IMAGE" &&
      tag="$DL_BUILDAH_TAG" &&
      context="${DL_BUILDAH_CONTEXT:-.}"
  } ||
    {
      cmnFail "Failed to set values for required local variables."
      return 4
    }

  {
    add_custom_ca_certs &&
    login_to_primary_registry &&
    login_to_extra_registries &&
    build_and_push_image
  } || return "$?" 

} 


main ||
  {
    exit_code="${?}"
    echo -e "\n[FAIL] $0 | Entrypoint script failed; exiting ${exit_code}." 1>&2
    exit "${exit_code}"
  }
