#  Gitleaks Included Pipeline

## Purpose

This pipeline runs [gitleaks](https://github.com/zricethezav/gitleaks) on the repository to check for unencrypted secrets.
