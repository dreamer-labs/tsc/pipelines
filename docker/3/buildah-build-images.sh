#!/usr/bin/env bash

# @description
#    This is the entrypoint for any jobs that use `.build_images_*` as their template
#    in the `gitlab-ci.yml` for the `docker` pipeline.
main() {
  local pipelines_prefix;
  pipelines_prefix=''
  (
    # This is to set up builds for local build pipelines only
    [[ -f libraries/4/commonFuncs.lib.sh ]] || pipelines_prefix="/pipelines/"

    source "${pipelines_prefix}libraries/4/commonFuncs.lib.sh" &&
      source "${pipelines_prefix}libraries/4/osFuncs.lib.sh" || exit 1

    {
      local target &&
        local dockerfile &&
        local ci_registry_user &&
        local ci_registry_password &&
        local ci_registry &&
        local ci_extra_registries &&
        local ci_registry_image &&
        local tag
    } || cmnFail "Failed to declare required local variables." || exit 1

    {
      cmnAssertSet CI_REGISTRY_USER &&
        cmnAssertSet CI_REGISTRY_PASSWORD &&
        cmnAssertSet CI_REGISTRY &&
        cmnAssertSet CI_REGISTRY_IMAGE
        cmnAssertSet TARGET &&
        cmnAssertSet TAG
    } || cmnFail "Failed to assert required variables." || exit 1

    {
      target="$TARGET" &&
        dockerfile="${DOCKERFILE:-Dockerfile}" &&
        ci_registry_user="$CI_REGISTRY_USER" &&
        ci_registry_password="$CI_REGISTRY_PASSWORD" &&
        ci_registry="$CI_REGISTRY" &&
        ci_extra_registries=$CI_EXTRA_REGISTRIES &&
        ci_registry_image="$CI_REGISTRY_IMAGE" &&
        tag="$TAG"
    } || cmnFail "Failed to set values for required local variables." || exit 1

    {
      # Only for private runners against a private gitlab instance (e.g. our repos)
      if [[ -f "/etc/gitlab-runner/certs/ca.crt" ]];
      then
        osAddCACertificate /etc/gitlab-runner/certs/ca.crt || cmnFail "Failed to update-ca-trust for detected gitlab-runner cert" || exit 1
      fi
      {
        buildah login -u "$ci_registry_user" -p "$ci_registry_password" "$ci_registry" &&
          cmnInfo "Logged in to: $ci_registry_user:masked@$ci_registry" ||
          cmnFail "Failed to login to: $ci_registry_user:masked@$ci_registry"
      } || exit 1

      if [[ -n "${ci_extra_registries}" ]];
      then
        (
          IFS=","
          for registry in $ci_extra_registries; do
            {
              [[ "$registry" =~ ^(.+):(.+)@(.+)$ ]] &&
              buildah login -u "${BASH_REMATCH[1]}" -p "${BASH_REMATCH[2]}" "${BASH_REMATCH[3]}" &&
                cmnInfo "Logged in to: ${BASH_REMATCH[1]}:masked@${BASH_REMATCH[3]}" ||
                cmnFail "Failed to login to: ${BASH_REMATCH[1]}:masked@${BASH_REMATCH[3]}" || exit 1
            }
          done
        ) || cmnFail "Failed to auth to a defined extra registry from CI_EXTRA_REGISTRIES" || exit 1
      fi
    } || cmnFail "Failure during registry login" || exit 1

    {
      buildah bud -t "$ci_registry_image/$target:${tag}" --target "$target" --format docker -f "$dockerfile" . &&
      buildah push "$ci_registry_image/$target:${tag}"
    } || cmnFail "Failed buildah bud and push process" || exit 1

  ) || return 1

}

main ||
  {
    echo -e "\n[FAIL] Failed with unhandled error." 1>&2
    exit 1
  }
