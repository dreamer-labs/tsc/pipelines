# Docker Included Pipeline

## Purpose

This pipeline builds OCI images from dockerfiles using Docker

This pipeline does not run any jobs. It only provides some job templates.

It also includes sanity checking on the environment to ensure that required variables were set.

### Example Include With Job

```
build_ansible_base_image:
  extends:
    - .dl_docker_build_image
    - .dl_docker_base_image
  variables:
    DL_DOCKER_TARGET: ansible-base
    DL_DOCKER__DOCKERFILE: path/to/your/Dockerfile
    DL_DOCKER_TAG: latest
```

## Input Variables

### DL\_DOCKER\_DOCKERFILE

Purpose:

- The name of the docker file that you would like the pipeline to build an image from

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI

Requirements:

- Variable defaults to Dockerfile of container's CWD

### DL\_DOCKER\_TARGET

Purpose:

- The target name inside the docker file that you would like the pipeline to build

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI
- This variable's value is also used as the image name in the docker registry

Requirements:

- Variable must be set
- Value should have a length greater than zero
- Value must include at least one non-whitespace character
- Target name must exist inside the dockerfile

### DL\_DOCKER\_TAG

Purpose:

- The tag name that you would used when pushing the image to the docker registry

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI
- Defaults to "mr-merge-request-number", where "mr-" is literal, and "merge-request-number" is the MR# for the MR where the image was built during
- Using the MR number ensures that each image tag is unique
- Once the MR is merged, the MR-tagged development image is now considered latest/production/stable

Requirements:

- Variable must be set
- Value must have a length greater than zero
- Value must include at least one non-whitespace character
- Target name must exist inside the dockerfile

### CI_REGISTRY_USER

Purpose:

- The registry user with permissions to push/pull images to the docker registry that will store the built images

Usage:

- This variable is automatically set by GitlabCI by default
- Can be overwritten in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI
- If images will be stored in the same repository where they are built: do not set manually

Requirements:

- Variable must be set
- Value must have a length greater than zero
- Value must include at least one non-whitespace character

### CI\_REGISTRY\_PASSWORD

Purpose:

- The registry token with permissions to push/pull images to the docker registry that will store the built images

Usage:

- This variable is automatically set by GitlabCI by default
- Can be overwritten in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI
- If images will be stored in the same repository where they are built: do not set manually

Requirements:

- Variable must be set
- Value must have a length greater than zero
- Value must include at least one non-whitespace character

### CI\_REGISTRY

Purpose:

- The location of the docker registry that will store the built images

Usage:

- This variable is automatically set by GitlabCI by default
- Can be overwritten in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI
- If images will be stored in the same repository where they are built: do not set manually

Requirements:

- Variable must be set
- Value must have a length greater than zero
- Value must include at least one non-whitespace character
- Must explictly include the protocol (i.e. https://) and port (i.e. :8080 ), when applicable
- Must be a fully-qualified, valid url string

### CI\_REGISTRY\_IMAGE

Purpose:

- The location of the docker registry (plus image name) where the built images will be stored

Usage:

- This variable is automatically set by GitlabCI by default
- Can be overwritten in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI
- If images will be stored in the same repository where they are built: do not set manually

Requirements:

- Variable must be set
- Value must have a length greater than zero
- Value must include at least one non-whitespace character
- Must include the protocol (i.e. https://) and port (i.e. :8080 ), when applicable
- Must be a fully-qualified, valid url string
- Must include the image name after the registry server url string (i.e. /myimagenamehere)

### CI\_EXTRA\_REGISTRIES

Purpose:

- A camma separated string of additional registry connections in standard user:pass@host.com format

Example: `user1:pass1@host.com,user2:pass2@host2.com`

Requirments:

- Value must have a length greater than zero
- Value must have at least 1 connection defined but may have more
- Value must include at least one non-whitespace character
- Value may include a port (i.e. :8080 ), when applicable
- Must be a fully-qualified, valid url stringa
