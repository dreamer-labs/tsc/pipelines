#!/usr/bin/env bash

# @description
#    This is the entrypoint for any jobs that use `.build_images_*` as their template
#    in the `gitlab-ci.yml` for the `docker` pipeline.
main() {

  (
    local libs_version &&
      libs_version="2.1.0" &&
      source libraries/${libs_version}/commonFuncs.lib.sh || return 1

    {
      local target &&
        local dockerfile &&
        local ci_registry_user &&
        local ci_registry_password &&
        local ci_registry &&
        local ci_registry_image &&
        local devel_slug
    } || cmnFail "Failed to declare required local variables."

    {
      target="$TARGET" &&
        dockerfile="$DOCKERFILE" &&
        ci_registry_user="$CI_REGISTRY_USER" &&
        ci_registry_password="$CI_REGISTRY_PASSWORD" &&
        ci_registry="$CI_REGISTRY" &&
        ci_registry_image="$CI_REGISTRY_IMAGE" &&
        devel_slug="$DEVEL_SLUG"
    } || cmnFail "Failed to set values for required local variables."

    {
      docker login -u "$ci_registry_user" -p "$ci_registry_password" "$ci_registry" &&
        DOCKER_BUILDKIT=1 docker build --pull -t "${ci_registry_image}/${target}:${devel_slug}" -f "$dockerfile" . &&
        docker push "${ci_registry_image}/${target}:${devel_slug}"
    } || cmnFail "Failed with unhandled error."

  ) || exit 1

}

main ||
  {
    echo -e "\n[FAIL] Failed with unhandled error." 1>&2
    exit 1
  }
