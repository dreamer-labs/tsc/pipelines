#!/usr/bin/env bash

# @name commonFuncs.lib.sh
# @brief A library for common functions shared by all pipelines
# @description
#    This library provides:
#      - Functions for pretty printing stdout/stderr
#      - Functions for input validations and setting helpful variables

# @description
#    Used by function developers to output messages in a consistent way
#    Prints a debug message with blue text to stdout and returns 0
debug() {

  if [[ "$DEBUG" == "true" ]]; then
    local message

    message="$1"
    calling_function="${FUNCNAME[1]:-shell}" # default to "shell" if not called by a function

    hash tput &>/dev/null || debug "Optional util (tput) not found in path."
    # Why "xterm-color" and not "xterm" you ask? https://github.com/rburns/ansi-to-html/issues/25
    tput -T xterm-color setaf 4 2>/dev/null # sets text color to blue
    echo -e "\n[DEBUG] ${calling_function} | ${message}"
    tput -T xterm-color sgr0 2>/dev/null # removes all formatting
  fi
  return 0

}


# @description
#    Used by function developers to output messages in a consistent way
#    Prints an info message with green text to stdout and returns 0
info() {

  local message

  message="$1"
  calling_function="${FUNCNAME[1]:-shell}" # default to "shell" if not called by a function

  hash tput &>/dev/null || debug "Optional util (tput) not found in path."
  # Why "xterm-color" and not "xterm" you ask? https://github.com/rburns/ansi-to-html/issues/25
  tput -T xterm-color setaf 2 2>/dev/null # sets text color to green
  echo -e "\n[INFO] ${calling_function} | ${message}"
  tput -T xterm-color sgr0 2>/dev/null # removes all formatting
  return 0

}


# @description
#    Used by function developers to output messages in a consistent way
#    Prints a warning message with yellow text for non-fatal errors to stderr and returns 0
warn() {

  local message

  message="$1"
  calling_function="${FUNCNAME[1]:-shell}" # default to "shell" if not called by a function

  hash tput &>/dev/null || debug "Optional util (tput) not found in path."
  # Why "xterm-color" and not "xterm" you ask? https://github.com/rburns/ansi-to-html/issues/25
  tput -T xterm-color setaf 3 2>/dev/null # sets text color to yellow
  echo -e "\n[WARN] ${calling_function} | ${message}" 1>&2
  tput -T xterm-color sgr0 2>/dev/null # removes all formatting
  return 0

}


# @description
#    Used by function developers to output messages in a consistent way
#    Prints a failure message with red text for fatal errors to stderr and returns 1
fail() {

  local message
  local calling_function

  message="$1"
  calling_function="${FUNCNAME[1]:-shell}" # default to "shell" if not called by a function

  hash tput &>/dev/null || debug "Optional util (tput) not found in path."
  # Why "xterm-color" and not "xterm" you ask? https://github.com/rburns/ansi-to-html/issues/25
  tput -T xterm-color setaf 1 2>/dev/null # sets text color to red
  echo -e "\n[FAIL] ${calling_function} | ${message}\n" 1>&2
  tput -T xterm-color sgr0 2>/dev/null # removes all formatting
  exit 1

}


# @description
#    Ensures a variable is set to a minimally useable value
#    Expects name of variable (not value of variable) as $1
#    Outputs a return code to indicate variable viability
assert_set() {

  local -n nameref_of_var

  nameref_of_var="$1"

  # Fails if variable is unset, length is zero, or contains only whitespace
  [[ -n "${nameref_of_var//[[:blank:]]}" ]] ||
  {
    fail "Variable ($1) not set, empty, or only whitespace."
    return 1
  }

}


# @description
#    Prints absolute path of directory containing script that calls this function
#    Can be used to set a variable for consistent execution context
get_script_dir() {

  local relative_path_of_script

  relative_path_of_script="${BASH_SOURCE[0]}"

  for util in dirname realpath; do
    hash "$util" &>/dev/null || fail "Required util ($util) not found in path."
  done
  dirname "$(realpath "${relative_path_of_script}")"

}
