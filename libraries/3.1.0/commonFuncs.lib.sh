#!/usr/bin/env bash

# @name commonFuncs.lib.sh
# @brief Common functions library
# @description
#    A library for common functions shared by all pipelines

# @description
#    This function is a template for new functions
cmnFunctionTemplate() {

  {
    {
      local missing_libs &&
        missing_libs=0 &&
        for func in cmnFail cmnDebug; do
          hash $func &> /dev/null ||
            {
              echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
              missing_libs=1
            }
        done &&
        { [[ "$missing_libs" == "0" ]] || return 1; }
    } || {
      echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
      return 1
    }
  } &&
    {
      {
        local variableone &&
          local variabletwo &&
          local variablethree
      } || cmnFail "Variable declaration phase failed."
    } &&
    {
      {
        variableone="valueone" &&
          variabletwo="valuetwo" &&
          variablethree="valuethree"
      } || cmnFail "Variable assignment phase failed."
    } &&
    {
      {
        { echo "variableone: $variableone" || echo "Failed to echo variableone"; } &&
          echo "variabletwo: $variabletwo" &&
          echo "variablethree: $variablethree"
      } || cmnFail "Function execution phase failed."
    }

}

# @description
#    Used by function developers to output messages in a consistent way
#    Prints a debug message with blue text to stdout and returns 0
cmnDebug() {

  if [[ "$PIPELINE_DEBUG" != "true" ]]; then
    return 0 # exit function here if debugging if not enabled with PIPELINE_DEBUG=true
  fi || {
    echo -e "\n[FAIL] ${FUNCNAME[0]} | Conditional execution check phase failed.\n"
    return 1
  }

  {
    {
      {
        hash cmnFail &> /dev/null ||
          {
            echo -e "\n[FAIL] Required library func (cmnFail) not sourced.\n" 1>&2
            return 1
          }
      }
    } || {
      echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
      return 1
    }
  } &&
    {
      {
        local message &&
          local calling_function
      } || cmnFail "Variable declaration phase failed."
    } &&
    {
      {
        { { [[ -n "$1" ]] && message="$1"; } || cmnFail "Failed to pass a message to print."; } &&
          calling_function="${FUNCNAME[1]:-shell}" # default to "shell" if not called by a function
      } || cmnFail "Variable assignment phase failed."
    } &&
    {
      {
        hash tput &> /dev/null || echo -e "\n[DEBUG] cmnDebug | Optional util (tput) not found in path." &&
          # Why "xterm-color" and not "xterm" you ask? https://github.com/rburns/ansi-to-html/issues/25
          # 4 sets text color to blue
          tput -T xterm-color setaf 4 2> /dev/null &&
          echo -e "\n[DEBUG] $calling_function | $message" &&
          # sgr0 removes all formatting
          tput -T xterm-color sgr0 2> /dev/null
      } || cmnFail "Function execution phase failed."
    }

}

# @description
#    Used by function developers to output messages in a consistent way
#    Prints an info message with green text to stdout and returns 0
cmnInfo() {

  {
    {
      local missing_libs &&
        missing_libs=0 &&
        for func in cmnFail cmnDebug; do
          hash $func &> /dev/null ||
            {
              echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
              missing_libs=1
            }
        done &&
        { [[ "$missing_libs" == "0" ]] || return 1; }
    } || {
      echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
      return 1
    }
  } &&
    {
      {
        local message &&
          local calling_function
      } || cmnFail "Variable declaration phase failed."
    } &&
    {
      {
        { { [[ -n "$1" ]] && message="$1"; } || cmnFail "Failed to pass a message to print."; } &&
          # default to "shell" if not called by a function
          calling_function="${FUNCNAME[1]:-shell}"
      } || cmnFail "Variable assignment phase failed."
    } &&
    {
      {
        { hash tput &> /dev/null || cmnDebug "Optional util (tput) not found in path."; } &&
          # Why "xterm-color" and not "xterm" you ask? https://github.com/rburns/ansi-to-html/issues/25
          # 2 sets text color to green
          tput -T xterm-color setaf 2 2> /dev/null &&
          echo -e "\n[INFO] $calling_function | $message" &&
          # sgr0 removes all formatting
          tput -T xterm-color sgr0 2> /dev/null &&
          return 0
      } || cmnFail "Function execution phase failed."
    }

}

# @description
#    Used by function developers to output messages in a consistent way
#    Prints a warning message with yellow text for non-fatal errors to stderr and returns 0
cmnWarn() {

  {
    {
      local missing_libs &&
        missing_libs=0 &&
        for func in cmnFail cmnDebug; do
          hash $func &> /dev/null ||
            {
              echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
              missing_libs=1
            }
        done &&
        { [[ "$missing_libs" == "0" ]] || return 1; }
    } || {
      echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
      return 1
    }
  } &&
    {
      {
        local message &&
          local calling_function
      } || cmnFail "Variable declaration phase failed."
    } &&
    {
      {
        { { [[ -n "$1" ]] && message="$1"; } || cmnFail "Failed to pass a message to print."; } &&
          # default to "shell" if not called by a function
          calling_function="${FUNCNAME[1]:-shell}"
      } || cmnFail "Variable assignment phase failed."
    } &&
    {
      {
        hash tput &> /dev/null || cmnDebug "Optional util (tput) not found in path." &&
          # Why "xterm-color" and not "xterm" you ask? https://github.com/rburns/ansi-to-html/issues/25
          # 3 sets text color to yellow
          tput -T xterm-color setaf 3 2> /dev/null &&
          echo -e "\n[WARN] $calling_function | $message" 1>&2 &&
          # sgr0 removes all formatting
          tput -T xterm-color sgr0 2> /dev/null &&
          return 0
      } || cmnFail "Function execution phase failed."
    }

}

# @description
#    Used by function developers to output messages in a consistent way
#    Prints a failure message with red text for fatal errors to stderr and returns 1
cmnFail() {

  {
    {
      {
        hash cmnDebug &> /dev/null ||
          {
            echo -e "\n[FAIL] Required library func (cmnDebug) not sourced.\n" 1>&2
            return 1
          }
      }
    } || {
      echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
      return 1
    }
  } &&
    {
      {
        local message &&
          local calling_function
      } || {
        echo -e "\n[FAIL] ${FUNCNAME[0]} | Variable declaration phase failed.\n" 1>&2
        return 1
      }
    } &&
    {
      {
        { { [[ -n "$1" ]] && message="$1"; } || cmnFail "Failed to pass a message to print."; } &&
          calling_function="${FUNCNAME[1]:-shell}" # default to "shell" if not called by a function
      } || {
        echo -e "\n[FAIL] ${FUNCNAME[0]} | Variable assignment phase failed.\n" 1>&2
        return 1
      }
    } &&
    {
      {
        hash tput &> /dev/null || cmnDebug "Optional util (tput) not found in path." &&
          # Why "xterm-color" and not "xterm" you ask? https://github.com/rburns/ansi-to-html/issues/25
          # 1 sets text color to red
          tput -T xterm-color setaf 1 2> /dev/null &&
          echo -e "\n[FAIL] $calling_function | ${message}\n" 1>&2 &&
          # sgr0 removes all formatting
          tput -T xterm-color sgr0 2> /dev/null &&
          return 1
      } || {
        echo -e "\n[FAIL] ${FUNCNAME[0]} | Function execution phase failed.\n" 1>&2
        return 1
      }
    }

}

# @description
#    Ensures a variable is set to a minimally useable value
#    Expects name of variable (not value of variable) as $1
#    Outputs a return code to indicate variable viability
cmnAssertSet() {

  {
    {
      local missing_libs &&
        missing_libs=0 &&
        for func in cmnFail cmnDebug; do
          hash $func &> /dev/null ||
            {
              echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
              missing_libs=1
            }
        done &&
        { [[ "$missing_libs" == "0" ]] || return 1; }
    } || {
      echo -e "\n[FAIL] Library sourcing phase failed.\n" 1>&2
      return 1
    }
  } &&
    {
      {
        local -n nameref_of_var
      } || cmnFail "Variable declaration phase failed."
    } &&
    {
      {
        { [[ -n "$1" ]] && nameref_of_var="$1"; } || cmnFail "Failed to pass valid variable name."
      } || cmnFail "Variable assignment phase failed."
    } &&
    {
      {
        # Fails if variable is unset, length is zero, or contains only whitespace
        [[ -n "${nameref_of_var//[[:blank:]]/}" ]] || cmnFail "Variable ($1) not set, empty, or only whitespace."
      } || cmnFail "Function execution phase failed."
    }

}

# @description
#    Ensures that the contents of a passed-in variable name ($1)
#    match a passed-in regex string ($2).
cmnAssertValid() {

  {
    {
      local missing_libs &&
        missing_libs=0 &&
        for func in cmnFail cmnDebug; do
          hash $func &> /dev/null ||
            {
              echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
              missing_libs=1
            }
        done &&
        { [[ "$missing_libs" == "0" ]] || return 1; }
    } || {
      echo -e "\n[FAIL] Library sourcing phase failed.\n" 1>&2
      return 1
    }
  } &&
    {
      {
        local -n nameref_of_var &&
          local regex_to_match
      } || cmnFail "Variable declaration phase failed."
    } &&
    {
      {
        { { [[ -n "$1" ]] && nameref_of_var="$1"; } || cmnFail "Failed to pass valid variable name."; } &&
          { { [[ -n "$2" ]] && regex_to_match="$2"; } || cmnFail "Failed to pass regex string to match."; }
      } || cmnFail "Variable assignment phase failed."
    } &&
    {
      {
        cmnDebug "Checking variable ($1) contents ($nameref_of_var) against regex ($regex_to_match)." &&
          { [[ "$nameref_of_var" =~ $regex_to_match ]] || cmnFail "Variable ($1) contents ($nameref_of_var) DO NOT match the regex ($regex_to_match)."; } &&
          cmnDebug "Variable ($1) contents ($nameref_of_var) DO match (${BASH_REMATCH[0]}) the regex ($regex_to_match)."
      } || cmnFail "Function execution phase failed."
    }

}

# @description
#    Ensures that the content of a namereferenced variable ($1)
#    is the filepath to a valid filesystem object (i.e. file, directory, socket, et cetera).
cmnAssertExists() {

  {
    {
      local missing_libs &&
        missing_libs=0 &&
        for func in cmnFail cmnDebug; do
          hash $func &> /dev/null ||
            {
              echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
              missing_libs=1
            }
        done &&
        { [[ "$missing_libs" == "0" ]] || return 1; }
    } || {
      echo -e "\n[FAIL] Library sourcing phase failed.\n" 1>&2
      return 1
    }
  } &&
    {
      {
        local -n filesystem_object
      } || cmnFail "Variable declaration phase failed."
    } &&
    {
      {
        { [[ -n "$1" ]] && filesystem_object="$1"; } || cmnFail "Failed to pass in the name of a var containing the filepath of a fs object to check."
      } || cmnFail "Variable assignment phase failed."
    } &&
    {
      {
        cmnDebug "Checking variable ($1) to see if it contains the filepath to a valid filesystem object." &&
          { [[ -e "$filesystem_object" ]] || cmnFail "Variable ($1) does not contain ($filesystem_object) the value of a valid object on the filesystem."; } &&
          cmnDebug "Item ($1) is a valid object on the filesystem."
      } || cmnFail "Function execution phase failed."
    }

}

# @description
#    Ensures that the content of a namereferenced variable ($1)
#    Are also found in a space-delimited list ($2).
#    Note: only allows $1 and $2 to contain certain characters
cmnAssertInList() {

  {
    local missing_libs &&
      missing_libs=0 &&
      for func in cmnFail cmnDebug; do
        hash $func &> /dev/null ||
          {
            echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
            missing_libs=1
          }
      done &&
      { [[ "$missing_libs" == "0" ]] || {
        echo -e "\n[FAIL] Library sourcing phase failed.\n" 1>&2
        return 1
      }; }
  } &&
    {
      {
        local list_to_check_in &&
          local -n item_to_check
      } || cmnFail "Variable declaration phase failed."
    } &&
    {
      {
        { { [[ -n "$2" ]] && list_to_check_in="$2"; } || cmnFail "Failed to provide list to check item against."; } &&
          { { [[ -n "$1" ]] && item_to_check="$1"; } || cmnFail "Failed to provide item to lookup in list."; } &&
          { [[ "${list_to_check_in}" == "${list_to_check_in//[^[:blank:][:alnum:]-]/}" ]] || cmnFail "List ($list_to_check_in) may only contain alphanumeric, dash, and blank characters."; } &&
          { [[ "${item_to_check}" == "${item_to_check//[^[:alnum:]-]/}" ]] || cmnFail "Item to check ($1) may only contain ($item_to_check) alphanumeric and dash characters."; }
      } || cmnFail "Variable assignment phase failed."
    } &&
    {
      {
        for list_item in ${list_to_check_in}; do
          if [[ "${item_to_check}" == "${list_item}" ]]; then
            cmnDebug "Item ($item_to_check) matches item in list ($list_item)."
            return 0
          fi
        done
        cmnFail "Failed to find matching item in list that matches item ($item_to_check)."
      } || cmnFail "Function execution phase failed."
    }

}

# @description
#    Prints absolute path of directory containing script that calls this function
#    Can be used to set a variable for consistent execution context
cmnGetScriptDir() {

  {
    {
      local missing_libs &&
        missing_libs=0 &&
        for func in cmnFail cmnDebug; do
          hash $func &> /dev/null ||
            {
              echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
              missing_libs=1
            }
        done &&
        { [[ "$missing_libs" == "0" ]] || return 1; }
    } || {
      echo -e "\n[FAIL] Library sourcing phase failed.\n" 1>&2
      return 1
    }
  } &&
    {
      {
        local relative_path_of_script
      } || cmnFail "Variable declaration phase failed."
    } &&
    {
      {
        relative_path_of_script="${BASH_SOURCE[0]}"
      } || cmnFail "Variable assignment phase failed."
    } &&
    {
      {
        for util in dirname realpath; do
          hash "$util" &> /dev/null || cmnFail "Required util ($util) not found in path."
        done &&
          dirname "$(realpath "$relative_path_of_script")"
      } || cmnFail "Function execution phase failed."
    }

}

# @description
#    Given $DEPLOYMENT, this function checks $DEPLOYMENT against a list of known good prefixes
#    and returns just the deployment environment name suffix.
#
#    For example, given:
#      - DEPLOYMENT="staging-mysupertest"
#      - VALID_ENV_PREFIXES="staging prod super-runner"
#
#    The function would return:
#      - "staging"
cmnGetDeploymentEnvPrefix() {

  {
    {
      local missing_libs &&
        missing_libs=0 &&
        for func in cmnFail cmnDebug; do
          hash $func &> /dev/null ||
            {
              echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
              missing_libs=1
            }
        done &&
        { [[ "$missing_libs" == "0" ]] || return 1; }
    } || {
      echo -e "\n[FAIL] Library sourcing phase failed.\n" 1>&2
      return 1
    }
  } &&
    {
      {
        local valid_env_prefixes &&
          local full_deployment_name &&
          local env_prefix_match_found &&
          local matching_prefix &&
          local deployment_name_prefix
      } || cmnFail "Variable declaration phase failed."
    } &&
    {
      {
        valid_env_prefixes="$VALID_ENV_PREFIXES"
        full_deployment_name="$DEPLOYMENT"
      } || cmnFail "Variable assignment phase failed."
    } &&
    {
      {
        for valid_env_prefix in ${valid_env_prefixes}; do
          # Checks if deployment name starts with any valid prefixes (plus a "-" delimiter)
          if [[ "${full_deployment_name}" =~ ^${valid_env_prefix}- ]]; then
            {
              env_prefix_match_found="1" &&
                matching_prefix="${BASH_REMATCH[0]}" &&
                # ":0: -1" drops the dash delimiter at the end of the regex match
                deployment_name_prefix="${matching_prefix:0:-1}" &&
                echo "$deployment_name_prefix"
            } || return 1
          fi
        done &&
          if [[ "$env_prefix_match_found" != "1" ]]; then
            cmnFail "Deployment name ($full_deployment_name) did not contain any known valid env prefixes." || return 1
          fi
      } || cmnFail "Function execution phase failed."
    }

}

# @description
#    Given $DEPLOYMENT, this function checks $DEPLOYMENT against a list of known good prefixes
#    and returns just the deployment environment name suffix.
#
#    For example, given:
#      - DEPLOYMENT="staging-mysupertest"
#      - VALID_ENV_PREFIXES="staging prod super-runner"
#
#    The function would return:
#      - "mysupertest"
cmnGetDeploymentEnvSuffix() {

  {
    {
      local missing_libs &&
        missing_libs=0 &&
        for func in cmnFail cmnDebug; do
          hash $func &> /dev/null ||
            {
              echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
              missing_libs=1
            }
        done &&
        { [[ "$missing_libs" == "0" ]] || return 1; }
    } || {
      echo -e "\n[FAIL] Library sourcing phase failed.\n" 1>&2
      return 1
    }
  } &&
    {
      {
        local valid_env_prefixes &&
          local full_deployment_name &&
          local env_prefix_match_found &&
          local matching_prefix &&
          local deployment_name_suffix
      } || cmnFail "Variable declaration phase failed."
    } &&
    {
      {
        valid_env_prefixes="$VALID_ENV_PREFIXES" &&
          full_deployment_name="$DEPLOYMENT"
      } || cmnFail "Variable assignment phase failed."
    } &&
    {
      {
        for valid_env_prefix in ${valid_env_prefixes}; do
          # Checks if deployment name starts with any valid prefixes (plus a "-" delimiter)
          if [[ "${full_deployment_name}" =~ ^${valid_env_prefix}- ]]; then
            {
              env_prefix_match_found="1" &&
                matching_prefix="${BASH_REMATCH[0]}" &&
                # Strips the regex match from above from the deployment name to get the deployment suffix
                deployment_name_suffix="${full_deployment_name//$matching_prefix/}" &&
                echo "${deployment_name_suffix}"
            } || return 1
          fi
        done &&
          if [[ "$env_prefix_match_found" != "1" ]]; then
            cmnFail "Deployment name ($full_deployment_name) did not contain any known valid env prefixes."
          fi
      } || cmnFail "Function execution phase failed."
    }

}
