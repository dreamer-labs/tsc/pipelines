#!/usr/bin/env bash

# @name osFuncs.lib.sh
# @brief OS functions library
# @description
#    A library for handling OS differences shared by all pipelines

# @description
#    This function returns the distro as a string
osDistro() {
  (
  . /etc/os-release &&
  echo -n "$ID"
  )
}

# @description
#    This function returns the distro version
osVersion() {
  (
    . /etc/os-release &&
    echo -n "$VERSION_ID"
  )
}

# @description
#   This function installs a CA certificate to supported OS's
osAddCACertificate() {
  local cert_path;

  cert_path=$1
  cmnInfo "Installing CA Cert: $cert_path"

  case "$(osDistro)" in
    debian|ubuntu|alpine)
      {
        cp "$cert_path" /usr/local/share/ca-certificates/
        update-ca-certificates
      } || cmnFail "Failed to install CA Cert"
    ;;
    fedora|centos)
      {
        cp "$cert_path" /etc/pki/ca-trust/source/anchors/ &&
        update-ca-trust enable &&
        update-ca-trust extract
      } || cmnFail "Failed to install CA Cert"
    ;;
    *)
      cmnFail "Unsupported OS type $(osDistro)"
  esac
}
