# Pipeline Libraries

## Purpose

Each version of the libraries in this directory contains a set of library functions.
No code is executed in any of these files. They are strictly used for function definitions that are sourced in by entrypoint scripts called by the included pipelines.

## Usage

### Included Pipeline Users

Including pipelines will automatically include required libraries.
They are sourced in each entrypoint script and are not considered a public interface for you to worry about.
Read the docs for the included pipeline you are using to see the pipe's required inputs and expected outputs.


### Included Pipeline Developers

If you are writing an included pipeline and wish to include any functions in your entrypoint script, source them in like so at the top of your script in your main function:

```bash
  {
    source ../../libraries/1.0.0/commonFuncs.lib.sh &&
    source ../../libraries/2.0.1/terraformFuncs.lib.sh
  } || return 1
```

Each function should have a doc string that tells you what it does, why it does it, and the inputs/outputs.

### Function Developers

- Each function should have a doc string that tells you what it does, why it does it, and the inputs/outputs.
- These should be in `shdoc` format. (See: https://github.com/reconquest/shdoc#featureshttps://github.com/reconquest/shdoc).
- Avoid getting into details about *how* a function does something unless it is unconventional or complicated. For example: document any usage of regex, substring manipulations, or anything that you had to look up on Google/StackOverflow to write/fix.
- Avoid writing *how* a function works, and focus on providing pipeline developers with details on how, why, and when they would want to include your function in their entrypoint scripts.
- Avoid nesting or calling functions from other functions whenever possible; this allows entrypoint scripts to be as transparent as possible to anyone trying to understand their workflow and how they interact with backend libraries.
- Warn and continue on non-fatal errors, fail and exit on fatal ones, use info messages sparingly, but feel free to use conditionally-printed debug messages heavily for anyone that comes behind you to modify a function.
