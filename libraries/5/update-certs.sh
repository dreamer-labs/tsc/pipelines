#!/usr/bin/env bash

# @description
#    This is the entrypoint for the `update_certs` stand-alone script.
#    Rather than sourcing the function, this script can be called directly, like an entrypoint script.
main() {
  (
    {
      {
        source /pipelines/libraries/5/commonFuncs.lib.sh &&
          source /pipelines/libraries/5/osFuncs.lib.sh &&
          local missing_libs &&
          missing_libs=0 &&
          for func in cmnFail osAddCACertificate; do
            hash $func &>/dev/null ||
              {
                echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
                missing_libs=1
              }
          done &&
          { [[ "$missing_libs" == "0" ]] || return 1; }
      } || {
        echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
        return 1
      }
    } && {
      {
        if [[ -f "/etc/gitlab-runner/certs/ca.crt" ]]; then
          cmnInfo "Adding certificate found at /etc/gitlab-runner/certs/ca.crt"
          osAddCACertificate "/etc/gitlab-runner/certs/ca.crt"
        else
          cmnInfo "No custom certificates mounted at /etc/gitlab-runner/certs/ca.crt"
        fi
      } || cmnFail "Failed to add detected ssl certificate."
    }
  ) || return 1
}

main || {
  echo -e "\n[FAIL] $0 | Entrypoint script encountered a fatal error.\n" 1>&2
  exit 1
}
