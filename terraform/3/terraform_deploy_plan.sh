#!/usr/bin/env bash

# @description
#    This is the entrypoint for the `terraform_deploy_plan` jobs in the `gitlab-ci.yml` for the `terraform` pipeline.
main() {

  (

    {
      {
        source /pipelines/libraries/4/commonFuncs.lib.sh &&
          source /pipelines/terraform/3/terraFuncs.lib.sh &&
          local missing_libs &&
          missing_libs=0 &&
          for func in cmnFail cmnInfo cmnDebug tfGenerateBackendConfig tfCreateGitlabReport; do
            hash $func &> /dev/null ||
              {
                echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
                missing_libs=1
              }
          done &&
          { [[ "$missing_libs" == "0" ]] || return 1; }
      } || {
        echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
        return 1
      }
    } &&
      {
        {
          local deployment_env &&
            declare TF_VAR_project &&
            declare TF_VAR_env_tag &&
            declare TF_VAR_vpc_tag
        } || cmnFail "Variable declaration phase failed."
      } &&
      {
        {
          deployment_env="$(cmnGetDeploymentEnvPrefix)" &&
            # These TF_VAR_* vars are automatically imported by terraform when its called later during the execution phase
            TF_VAR_deployment="$DEPLOYMENT" &&
            readonly TF_VAR_deployment &&
            export TF_VAR_deployment &&
            TF_VAR_project="$CI_PROJECT_PATH" &&
            readonly TF_VAR_project &&
            export TF_VAR_project &&
            TF_VAR_env_tag="$(cmnGetDeploymentEnvPrefix)" &&
            readonly TF_VAR_env_tag &&
            export TF_VAR_env_tag &&
            TF_VAR_vpc_tag="$(cmnGetDeploymentEnvPrefix)-vpc" &&
            readonly TF_VAR_vpc_tag &&
            export TF_VAR_vpc_tag &&
            local terraform_args=("-var-file=../${deployment_env}.tfvars" "-input=false" "-lock=true" "-out=$TERRAFORM_PLAN")
        } || cmnFail "Variable assignment phase failed."
      } &&
      {
        {
          cd "$TERRAFORM_SRC_DIR" &&
            { [[ -f backend_conf.tf ]] || tfGenerateBackendConfig "$DEPLOYMENT" "$TERRAFORM_STATE_BUCKET" > backend_conf.tf; } &&
            { terraform init || cmnFail "Terraform init failed."; } &&
            {
              if [[ -n "$TERRAFORM_REBUILD_RESOURCE" ]]; then
                terraform state list "$TERRAFORM_REBUILD_RESOURCE" || cmnFail "Invalid resource name given for env var TERRAFORM_REBUILD_RESOURCE ($TERRAFORM_REBUILD_RESOURCE)" && \
                terraform_args+=("-replace=${TERRAFORM_REBUILD_RESOURCE}")
              fi;
            } &&
            {
              if [[ -f "${DEPLOYMENT}.tfvars" ]]; then
                terraform_args+=("-var-file=${DEPLOYMENT}.tfvars")
              fi;
            } &&
            cmnInfo "Running terraform plan with the following arguments: ${terraform_args[*]}" &&
            { terraform plan "${terraform_args[@]}" || cmnFail "Terraform plan failed."; } &&
            tfCreateGitlabReport <(terraform show --json 2> /dev/null) > "$TERRAFORM_PLAN_REPORT" &&
            cmnInfo "Checksum to APPLY this plan $(md5sum "$TERRAFORM_PLAN" 2> /dev/null)."
        } || cmnFail "Function execution phase failed."
      }

  ) || return 1

}

main || {
  echo -e "\n[FAIL] $0 | Entrypoint script encountered a fatal error.\n" 1>&2
  exit 1
}
