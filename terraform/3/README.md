#  Terraform (TF) Manual Included Pipeline

## Purpose

This pipeline performs terraform-related tasks (for manual deploys).

It also includes sanity checking on the environment to ensure that required variables were set.

## Input Variables

### DEPLOYMENT

Purpose:

- Unique name for a set of infrastructure

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI

Requirements:

- Variable must be set
- Value must have a length greater than zero
- Value must include at least one non-whitespace character


### DEPLOYMENT_TYPE

Purpose:

- Unique name for an operation (or set of operations) performed against a set of infrastructure

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI

Requirements:

- Variable must be set
- Value must have a length greater than zero
- Value must include at least one non-whitespace character

### CI_PROJECT_PATH

Purpose:

- Name of the project (Passed to terraform as 'TF_VAR_project' to tag which project created which Terraform resource)

Usage:

- Automatically set by GitlabCI
- If manually set, must be passed in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI

Requirements:

- Variable must be set
- Value must have a length greater than zero
- Value must include at least one non-whitespace character

### TERRAFORM_STATE_BUCKET

Purpose:

- Object store bucket name containing Terraform state file (Passed to terraform by dropping it into a terraform backend config file)

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI

Requirements:

- Variable must be set
- Value must have a length greater than zero
- Value must include at least one non-whitespace character

### TERRAFORM_SRC_DIR

Purpose:

- The location where terraform configuration files will be located inside of the docker container running the CI/CD job (Note: GitlabCI drops the code from the repo including this included pipeline into $CI_PROJECT_DIR, so this variable usually references a subdirectory of that directory.)

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI

Requirements:

- Variable must be set
- Value must have a length greater than zero
- Value must include at least one non-whitespace character

### VALID_ENV_PREFIXES

Purpose:

- A list of possible valid prefixes to allow in $DEPLOYMENT (full deployment name)

Usage:

- Must be passed in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI
- Must be a space-delimited list of accepted prefixes (i.e. staging, prod, super-runner, et cetera)
- These typically (for now) match the first part of our VPC names, and as a result,
  this var is indirectly used later to determine which VPC to deploy the infrastructure into

Requirements:

- Variable must be set
- Value must have a length greater than zero
- Value must include at least one non-whitespace character

### TERRAFORM_PLAN

Purpose:

- The filename where terraform will output its terraform plan into $TERRAFORM_SRC_DIR after generating the plan

Usage:

- Optionally passed in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI
- If set, must be a valid filename
- Set by default in included pipeline, but can be overwritten

Requirements:

- Variable must be set (either by the included pipeline or overwritten manually)
- Value must have a length greater than zero
- Value must include at least one non-whitespace character
- Must not include "/" characters

### TERRAFORM_PLAN_REPORT

Purpose:

- The filename where terraform will output a Gitlab Report based on its terraform plan into $TERRAFORM_SRC_DIR after generating the plan

Usage:

- Optionally passed in via the GitlabCI pipeline API, a GitlabCI file, or the Gitlab web UI
- If set, must be a valid filename
- Set by default in included pipeline, but can be overwritten

Requirements:

- Variable must be set (either by the included pipeline or overwritten manually)
- Value must have a length greater than zero
- Value must include at least one non-whitespace character
- Must not include "/" characters

### APPLY

Purpose:

- The md5sum of the terraform plan file generated via a 'terraform_*_plan' job. Used to confirm that the right plan file is actually applied during runs that require the user to manually trigger terraform_apply jobs from this pipeline. This ensures the wrong plan is not accidentally applied and that the right plan is intentional applied, when desired, not automatically.

Usage:

- Required to be passed in via the Gitlab web UI or via Gitlab API AFTER a terraform_*_plan job has been run
- If set, must be a valid md5sum matching the md5sum of the filename in $TERRAFORM_PLAN
- Not set by default anywhere, must be manually entered as a safety check

Requirements:

- Variable must be set (for terraform_apply job only)
- Value must have a length greater than zero
- Value must include at least one non-whitespace character

### DESTROY

Purpose:

- Triggers a destroy operation on the infrastructure. This will run a `terraform plan -destroy`

Usage:

- Required to be passed in via the Gitlab web UI or via Gitlab API
- Not set by default anywhere, must be manually entered as a safety check

Requirements:

- Variable must be set (for terraform_destroy_plan job only)
- Value must be `true`

### TERRAFORM_REBUILD_RESOURCE

Purpose:

- The Terraform resource to rebuild. If set this is passed to `terraform plan -replace=$TERRAFORM_REBUILD_RESOURCE`

Usage:

- Optionally set via the Gitlab web UI or the Gitlab API
- The value will be checked and must exist in the Terraform state file

Requirements:

- Value must point to a resource which exists in the Terraform state file

## Input Artifacts

### <env>.tfvars

Purpose:

- A Terraform variable file to use for deployment variables. This must be named after the deployment environment, which is taken from the first part (before the `-`) of the `$DEPLOYMENT` variable.

Requirements:

- Must exist at `../${DEPLOYMENT_ENV}.tfvars` where `${DEPLOYMENT_ENV}` is the environment name (`staging`, `prod`, etc)


### <DEPLOYMENT>.tfvars

Purpose:

- A terraform variable file to use for deployment variables. This must be named after the `$DEPLOYMENT` variable.
- Variables defined in this file will overwrite `<env>.tfvars` and `terraform.tfvars` variables.

Requirements:

- Must exist at `${DEPLOYMENT}.tfvars`

### Terraform files

Purpose:

- Files describing your terraform deployment. These must exist inside the `${TERRAFORM_SRC_DIR}` directory.

Requirements:

- Must exist inside `${TERRAFORM_SRC_DIR}`
- Collectively, the files must contain all the terraform resources you wish to manage
- Files must end in ".tf" to be slurped up by terraform and used as part of the terraform run
