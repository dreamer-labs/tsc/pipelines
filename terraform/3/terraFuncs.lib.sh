#!/usr/bin/env bash

# @description
#    This function accepts two arguments.
#    The first argument is the key of the tfstate file.
#    The second argument is the S3 bucket name which stores the tfstate file.
#    Generates a terraform backend config file pointing to the key and bucket given to the function.
tfGenerateBackendConfig() {

  {
    {
      local missing_libs &&
        missing_libs=0 &&
        for func in cmnFail cmnDebug; do
          hash $func &> /dev/null ||
            {
              echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
              missing_libs=1
            }
        done &&
        { [[ "$missing_libs" == "0" ]] || return 1; }
    } || {
      echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
      return 1
    }
  } &&
    {
      {
        local key &&
          local bucket
      } || cmnFail "Variable declaration phase failed."
    } &&
    {
      {
        { { [[ -n "$1" ]] && key="${1}"; } || cmnFail "Failed to pass key name to function."; } &&
          { { [[ -n "$2" ]] && bucket="${2}"; } || cmnFail "Failed to pass bucket name to function."; }
      } || cmnFail "Variable assignment phase failed."
    } &&
    {
      {
        echo "terraform {" &&
          echo "  backend \"s3\" {" &&
          echo "    bucket = \"${bucket}\"" &&
          echo "    key = \"${key}.tfstate\"" &&
          echo "    region = \"us-gov-west-1\"" &&
          echo "  }" &&
          echo "}"
      } || cmnFail "Function execution phase failed."
    }

}

# @description
#    This function accepts a terraform plan (in json format) from stdin,
#    and converts it into a format uploadable as a Gitlab Report.
#
#    - https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html
tfCreateGitlabReport() {

  {
    {
      {
        hash cmnWarn &> /dev/null ||
          {
            echo -e "\n[FAIL] Required library func (cmnWarn) not sourced.\n" 1>&2
            return 1
          }
      }
    } || {
      echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
      return 1
    }
  } &&
    {
      {
        { hash jq &> /dev/null || cmnWarn "Required util (jq) not found in path."; } &&
          {
            jq -r '([.resource_changes[]?.change.actions?]|flatten)|{"create":(map(select(.=="create"))|length),"update":(map(select(.=="update"))|length),"delete":(map(select(.=="delete"))|length)}' "$1" ||
              cmnWarn "Failed to convert terraform plan into a GitlabCI Report."
          }
      } || cmnWarn "Function execution phase failed with non-fatal error."
    }

}
