#!/usr/bin/env bash

# @description
#    This is the entrypoint for the `terraform_apply_*` jobs in the `gitlab-ci.yml` for the `terraform` pipeline.
main() {

  (

    {
      {
        source /pipelines/libraries/3.0.0/commonFuncs.lib.sh &&
          source /pipelines/terraform/1.0.0/terraFuncs.lib.sh &&
          local missing_libs &&
          missing_libs=0 &&
          for func in cmnFail cmnDebug tfGenerateBackendConfig; do
            hash $func &> /dev/null ||
              {
                echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
                missing_libs=1
              }
          done &&
          { [[ "$missing_libs" == "0" ]] || return 1; }
      } || {
        echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
        return 1
      }
    } &&
      {
        {
          declare TF_VAR_project &&
            declare TF_VAR_env_tag &&
            declare TF_VAR_vpc_tag
        } || cmnFail "Variable declaration phase failed."
      } &&
      {
        {
          # These TF_VAR_* vars are automatically imported by terraform when its called later during the execution phase
          TF_VAR_project="$CI_PROJECT_PATH" &&
            readonly TF_VAR_project &&
            export TF_VAR_project &&
            TF_VAR_env_tag="$(cmnGetDeploymentEnvPrefix)" &&
            readonly TF_VAR_env_tag &&
            export TF_VAR_env_tag &&
            TF_VAR_vpc_tag="$(cmnGetDeploymentEnvPrefix)-vpc" &&
            readonly TF_VAR_vpc_tag &&
            export TF_VAR_vpc_tag
        } || cmnFail "Variable assignment phase failed."
      } &&
      {
        {
          cd "$TERRAFORM_SRC_DIR" &&
            { [[ -f "backend_conf.tf" ]] || tfGenerateBackendConfig "$DEPLOYMENT" "$TERRAFORM_STATE_BUCKET" > backend_conf.tf; } &&
            cmnDebug "Looking for terraform plan file ($TERRAFORM_PLAN) with hash/md5sum: $APPLY" &&
            { hash md5sum &> /dev/null || cmnFail "Required util (md5sum) not found in path."; } &&
            # md5sum validation requires two spaces between hash and filename when using the busybox version
            # We addressed this we opted to install 'coreutils' into the alpine container to get GNU md5sum
            { md5sum -c <(echo "$APPLY  $TERRAFORM_PLAN") || cmnFail "Provided hash ($APPLY) did not match hash of tf plan ($TERRAFORM_PLAN); aborting deploy."; } &&
            { terraform init || cmnFail "Terraform init failed."; } &&
            { terraform show "$TERRAFORM_PLAN" || cmnFail "Terraform show failed."; } &&
            { terraform apply "$TERRAFORM_PLAN" || cmnFail "Terraform apply failed."; }
        } || cmnFail "Function execution phase failed."
      }

  ) || return 1

}

main || {
  echo -e "\n[FAIL] $0 | Entrypoint script encountered a fatal error.\n" 1>&2
  exit 1
}
