#!/usr/bin/env bash

# @description
#    Performs sanity checks on required environment variables
main() {

  {
    {
      source /pipelines/libraries/3.0.0/commonFuncs.lib.sh &&
        local missing_libs &&
        missing_libs=0 &&
        for func in cmnFail cmnAssertSet cmnAssertValid; do
          hash $func &> /dev/null ||
            {
              echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
              missing_libs=1
            }
        done &&
        { [[ "$missing_libs" == "0" ]] || return 1; }
    } || {
      echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
      return 1
    }
  } &&
    {
      {
        cmnAssertSet DEPLOYMENT_TYPE &&
          cmnAssertSet DEPLOYMENT &&
          cmnAssertSet CI_PROJECT_PATH &&
          cmnAssertSet TERRAFORM_STATE_BUCKET &&
          cmnAssertSet TERRAFORM_SRC_DIR &&
          cmnAssertSet VALID_ENV_PREFIXES &&
          # Var can only contain alphanumeric, dash, and blank characters
          cmnAssertValid VALID_ENV_PREFIXES '^[[:blank:][:alnum:]-]+$'
      } || cmnFail "Execution phase failed."
    }

}

main || {
  echo -e "\n[FAIL] $0 | Entrypoint script encountered a fatal error.\n" 1>&2
  exit 1
}
