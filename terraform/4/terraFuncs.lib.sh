#!/usr/bin/env bash

# @description
#    This function accepts two arguments.
#    The first argument is the key of the tfstate file.
#    The second argument is the S3 bucket name which stores the tfstate file.
#    Generates a terraform backend config file pointing to the key and bucket given to the function.
tfGenerateAWSBackendConfig() {

  {
    {
      local missing_libs &&
        missing_libs=0 &&
        for func in cmnFail cmnDebug; do
          hash $func &>/dev/null ||
            {
              echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
              missing_libs=1
            }
        done &&
        { [[ "$missing_libs" == "0" ]] || return 1; }
    } || {
      echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
      return 1
    }
  } &&
    {
      {
        local key &&
          local bucket
      } || cmnFail "Variable declaration phase failed."
    } &&
    {
      {
        { [[ $# -ne 2 ]] && cmnFail "Function requires 2 positional parameters, key ($1) and bucket ($2)"; } &&
          { { [[ -n "$1" ]] && key="${1}"; } || cmnFail "Failed to pass key name to function."; } &&
          { { [[ -n "$2" ]] && bucket="${2}"; } || cmnFail "Failed to pass bucket name to function."; }
      } || cmnFail "Variable assignment phase failed."
    } &&
    {
      {
        echo "terraform {" &&
          echo "  backend \"s3\" {" &&
          echo "    bucket = \"${bucket}\"" &&
          echo "    key = \"${key}.tfstate\"" &&
          echo "    region = \"us-gov-west-1\"" &&
          echo "  }" &&
          echo "}"
      } || cmnFail "Function execution phase failed."
    }

}

# @description
#    This function accepts two arguments.
#    The first argument is the key of the tfstate file.
#    The second argument is the S3 bucket name which stores the tfstate file.
#    Generates a terraform backend config file pointing to the key and bucket given to the function.
tfGenerateGitlabBackendConfig() {

  {
    {
      local missing_libs &&
        missing_libs=0 &&
        for func in cmnFail cmnDebug cmnInfo; do
          hash $func &>/dev/null ||
            {
              echo -e "\n[FAIL] Required library func ($func) not sourced.\n" 1>&2
              missing_libs=1
            }
        done &&
        { [[ "$missing_libs" == "0" ]] || return 1; }
    } || {
      echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
      return 1
    }
  } &&
    {
      {
        local deployment
      } || cmnFail "Variable declaration phase failed."
    } &&
    {
      {
        { [[ -n "$1" ]] && deployment="${1}"; } ||
          cmnFail "Failed to pass deployment name to function."
      } ||
        cmnFail "Variable declaration phase failed."
    } &&
    {
      {
        {
          {
            export TF_ADDRESS=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${deployment} &&
              export TF_HTTP_ADDRESS=${TF_ADDRESS} &&
              export TF_HTTP_LOCK_ADDRESS=${TF_ADDRESS}/lock &&
              export TF_HTTP_LOCK_METHOD=POST &&
              export TF_HTTP_UNLOCK_ADDRESS=${TF_ADDRESS}/lock &&
              export TF_HTTP_UNLOCK_METHOD=DELETE &&
              export TF_HTTP_USERNAME=gitlab-ci-token &&
              export TF_HTTP_PASSWORD=${CI_JOB_TOKEN} &&
              export TF_HTTP_RETRY_WAIT_MIN=5
          } || cmnFail "TF Variable declaration phase failed."
        } &&
          {
            cat <<SRC
terraform {
  backend "http" {
  }
}
SRC
          }
      } || cmnFail "Function execution phase failed."
    }
}

# @description
#    This function accepts a terraform plan (in json format) from stdin,
#    and converts it into a format uploadable as a Gitlab Report.
#
#    - https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html
tfCreateGitlabReport() {

  {
    {
      {
        hash cmnWarn &>/dev/null ||
          {
            echo -e "\n[FAIL] Required library func (cmnWarn) not sourced.\n" 1>&2
            return 1
          }
      }
    } || {
      echo -e "\n[FAIL] ${FUNCNAME[0]} | Library sourcing phase failed.\n" 1>&2
      return 1
    }
  } &&
    {
      {
        { hash jq &>/dev/null || cmnWarn "Required util (jq) not found in path."; } &&
          {
            jq -r '([.resource_changes[]?.change.actions?]|flatten)|{"create":(map(select(.=="create"))|length),"update":(map(select(.=="update"))|length),"delete":(map(select(.=="delete"))|length)}' "$1" ||
              cmnWarn "Failed to convert terraform plan into a GitlabCI Report."
          }
      } || cmnWarn "Function execution phase failed with non-fatal error."
    }

}
