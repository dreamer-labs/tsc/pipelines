# Dreamer Labs Pipelines

## Purpose

A curated set of pipelines and/or container images that can be included into your repository `.gitlab-ci.yml` to perform common infrastructure management tasks that include, (but are not limited to):

- Building Container images with Docker or Buildah
- Scanning repositories for secrets with Gitleaks
- Ensuring quality of Bash code with Shellcheck, Shfmt, and Shdoc
- Enforcing commit message conventions with Commitlint
- Checking compliance with security standards with OpenSCAP
- Linting GitlabCI files with Glab
- Enforcing Dockerfile best practices with Hadolint
- Provisioning infrastructure with Terraform
- Configuring infrastructure with Ansible

## Usage

GitlabCI provides an well-documented interface for including external pipelines in your `.gitlab-ci.yml`:

- https://docs.gitlab.com/ee/ci/yaml/includes.html

For each specific pipeline in this repository, the required variable inputs, file inputs, and directory structures are listed in the `README.md` inside each pipeline directory. Each README file should also explain when and why to use each pipeline.

### How to Upgrade to a Newer Version of an Included Pipeline

Each pipeline and library in this repository resides in a semantically versioned subdirectory structure.

- https://semver.org/

While semantic release has provisions for "minor" and "patch" versions, this repository only utilizes major versions, like API endpoints for a cloud/SaaS APIs do. We do push new features to pipelines and containers, but if we introduce a breaking change, we increment the pipeline/container version. This allows you, as the user to automatically inherit bug fixes and new features without referencing a new pipeline URL in the `.gitlab-ci.yml` pipeline code in your repository.

We will also issue deprecation warning as we plan to deprecate old stable versions of pipelines. This will be issued by adding a deprecation warning job to an existing stage in the pipeline. The job will fail, but will be marked `allow_failure: true` to get your attention in the GitlabCI pipelines interface.

Consider pipeline files, scripts, and libraries in these directories to be stable once added to the repository. Release candidates are  *not* stable, but are also *not* pushed to the `master` branch. As a result, always use `master` for deploying production infrastructure.

Before upgrading to a new pipeline version, review the `README.md` for that pipeline and update your variables, files, and other inputs to account for any new features or breaking changes, as needed.

If the new pipeline does not work, simply revert back to using the previous version that did work.
Then, notify that pipeline's developer(s) so that your use case can be taken into account in future releases.

## Contributing

### How to Update Existing (or Develop New) Pipelines

- Create a new feature/fix branch for your update off of the `master` branch
- Copy the latest version of an existing, similar pipeline and use it as a template for your update
- Create a MR that drops your update into a new semantically versioned subdirectory (i.e. pipename/3/)
- Creating an MR automatically creates new docker images with your new code in them
- Write smoke test case and fixtures to test your changes; use the test schema from an existing pipeline as an example
- Test the release candidate pipeline by running it against non-prod infrastructure
- Once sufficiently tested, have an approver review and merge your release candidate branch into `master`
- Once your branch is merged, your docker images (named after your MR number) are considered production

### How to Update Existing (or Develop New) Libraries

- Read the workflow for updating and creating new pipelines
- Follow a similar pattern, but:
  - Stage your library updates in the `libraries` directory in a new semantically versioned subdirectory
  - Ensure your MR includes at least one new pipeline version that uses your library to test your new library

### Documentation Guidelines

These repository has three main types of resources that need to be documented:

  - Included pipelines
  - Entrypoint scripts
  - Function libraries

Documentation for each resource has two audiences:

  - User of the resource
  - Maintainers of the resource


Each included pipeline should include a README.md for each version of the pipeline created.

This README should explain the purpose of the pipeline (high-level, in-general); for example:

  - What value the pipeline potential adds to a user considering using using it
  - Under what circumstances they would want to use it
  - Under what circumstances they would NOT use it

This README should also list out required and optional variables used by each job within the pipeline (if applicable). For each variable, explain the purpose, usage, and requirements. This README should also list out the required and optional files used by each job within the pipeline (if applicable); similar to the variables, explain the purpose, usage, and requirements.

In addition to the README, inline comments should be placed in the `gitlab-ci.yml` that briefly explain what each job does. One to two lines, max if possible.

In each entrypoint script, a backreference to the pipeline and job(s) calling it should be included at the top. Avoid rehashing the purpose of the job in this file since that should be listed in the `gitlab-ci.yml`.

Each function in the function library should explain what it does and why so that maintainers know/how when best to use it. All variables used by the functions should be local variables so that the global variables passed by the user in the GitlabCI files can be clearly tracked to their use in the functions. Namespacing and camelcasing are used for function naming to make it easy to track where entrypoint scripts are calling functions from.

## FAQ

**Q: What if I need an older version of a tool (i.e. ansible, terraform, etc.)**

A: Submit an MR for another `gitlab-ci.yml` that has a slightly different name, but lives in the same directory. For example, if `gitlab-ci.yml` uses an Ansible v4 image, but you need Ansible v3, make a copy of `gitlab-ci.yml` called `ansible-3.gitlab-ci.yml` that includes that older version as its `image:` and include that pipeline file instead in your repo's `.gitlab-ci.yml`.

**Q: How do I bump versions as I change/develop pipeline code?**

A: Use semantic versioning. The primary "public interfaces" in this case are the variables passed in to the pipelines. If a new variable is added, that is a minor version bump, add that to the current major release directory for the pipeline. If the variables do something different, one is deprecated, or one variable name replaces an existing one, that is a major change. If a different file structure or format is required to be in place in the repo calling the included pipeline you are developing, that is a breaking/major change. If only internal, user-transparent, refactoring is performed, that does not warrant a major version bump. If a major version bump is warranted, increment the major version directory structure to include a new version. Copy the code from the old latest major release, apply your changes, submit your MR for consideration.
