#!/usr/bin/env bash

main() {

  local -a excludes
  local -a results
  local -a failures
  local failed_files
  local gitlab_token
  local match

  gitlab_token="${DEPLOY_TOKEN}"
  match="false"
  failed_files="false"
  excludes+=( 'Getting contents in' )   # Always printed by glab
  excludes+=( 'Validating' )   # Always printed by glab
  excludes+=( '.yml is invalid' )   # Printed on lint failures, even false positives
  excludes+=( 'CI yml is Valid' )   # Printed when files pass glab ci lint
  excludes+=( 'unknown keys in ' )   # Linter can't see templates from other files
  excludes+=( 'jobs config should contain at least one visible job' )   # Linter doesn't like files with only templates
  excludes+=( 'does not have project' )   # Linter doesn't like local includes

  glab auth login --hostname gitlab.com --stdin <<<"$gitlab_token" ||
    {
      echo -e"\n[FAIL] Gitlab login failed." 1>&2
      return 1
    }
    for file in .gitlab-ci.y{,a}ml */*/*gitlab-ci*.y{,a}ml; do
      failures=()
      if [[ -f "$file" ]]; then
        mapfile results < <(glab -r dreamer-labs/tsc/pipelines ci lint "$file" 2>&1)
        for result in "${results[@]}"; do
          match="false"
          for exclude in "${excludes[@]}"; do
            if [[ "$result" =~ .*${exclude}.* ]]; then
              match="true"
            fi
          done
          if [[ "$match" != "true" ]] && [[ "$result" != "" ]]; then
            failures+=( "$result" )
          fi
        done
        if [[ "${#failures[@]}" != "" ]]; then
          for failure in "${failures[@]}"; do
            echo "$failure"
          done
          echo -e "\n[FAIL] File ($file) failed! :-(" 1>&2
          failed_files="true"
        else
          echo -e "\n[INFO] File ($file) passed! :-)"
        fi
      fi
    done
    if [[ "$failed_files" == "true" ]]; then
      echo -e "\n[FAIL] One or more CI files failed linting! :-(" 1>&2
      return 1
    fi

}

main || exit 1
