#!/usr/bin/env bash

main() {

  local fail

  fail=false

  for file in */*/*Dockerfile*; do
    echo -e "\n=== ${file} ==="
    echo -e "\n====== CONTENTS ======="
    while IFS= read -r line; do echo "$line"; done <"$file"
    echo -e "\n====== RESULTS ======"
    {
      hadolint \
	-t error \
	--trusted-registry "gitlab.com:443" \
	--trusted-registry "registry.gitlab.com" \
	--trusted-registry "quay.io" \
	"$file" &&
        echo -e "\n====== PASS ======\n"
    } ||
      {
        fail=true
        echo -e "====== FAIL ======\n"
      }
    echo -e "=== ${file} ===\n"
  done

  {
    [[ "$fail" == "false" ]] && echo -e "\n[INFO] All hadolint tests passed! :-)"; } ||
    {
      echo -e "\n[FAIL] One or more hadolint tests failed! :-(" 1>&2
      return 1
    }

}

main || exit 1
