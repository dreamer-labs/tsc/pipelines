#!/usr/bin/env bash

main() {

  local fail

  fail="false"

  for file in *.sh .gitlab-ci/*.sh */*/*.sh; do
    if [[ -f "$file" ]]; then
      echo -e "\n[INFO] Linting file ($file) with shellcheck."
      shellcheck -x "$file" || fail="true"
    fi
  done

  if [[ "$fail" == "true" ]]; then
    echo -e "\n[FAIL] One or more files failed shellcheck." 1>&2
    return 1
  fi

}

main || exit 1
