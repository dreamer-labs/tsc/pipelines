#!/usr/bin/env bash

main() {

  local -a dirs
  local report_dir

  {
    for dir in "${CI_PROJECT_DIR}"/.gitlab-ci/artifacts/*/kcov/; do
      echo -e "\n[INFO] Adding the following dir to list of reports to aggregate:" || return 1
      echo -e "         ${dir}" || return 1
      dirs+=("${dir}") || return 1
    done &&
      report_dir="${CI_PROJECT_DIR}/.gitlab-ci/artifacts/kcov_merge_reports/reports" &&
      echo -e "\n[INFO] Creating directory to aggregate reports:" &&
      echo -e "         ${report_dir}/" &&
      mkdir -p "${report_dir}/" &&
      echo -e "\n[INFO] Aggregating reports from the following directories:" &&
      for dir in "${dirs[@]}"; do
        echo -e "         ${dir}" || return 1
      done &&
      kcov --merge "${report_dir}/" "${dirs[@]}";
      while read -r line; do
        if [[ "${line}" =~ ^\"percent_covered.* ]]; then
          echo -e "\n[INFO] Total Code Covered: ${line//[^0-9.]}%"
        fi
      done < "${report_dir}/kcov-merged/coverage.json"
  } ||
    {
      echo -e "\n[FAIL] Failed to merge kcov reports." 1>&2
      return 1
    }

}

main || exit 1
