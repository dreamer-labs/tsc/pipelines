#!/usr/bin/env bash

check_var_namespaces() {

  local -a excludes
  local failures
  local line_count
  local file
  local -a env_vars
  local var_name
  local regex
  local match

  excludes+=( "CI_" )   # Gitlab_CI var prefixes
  excludes+=( "LOCAL_" )   # Local pipeline var prefixes
  excludes+=( "DL_" )   # Dreamer Labs pipeline var prefixes
  excludes+=( "PWD" )   # Authorized, dynamic, inherited shell vars/prefixes
  failures="false"
  line_count=0
  file="${1}"

  echo -e "\n[INFO] Checking file ($file) for adherence to current variable namespacing standards."
  readarray env_vars < <(printenv)
  while IFS= read -r line; do
    line_count="$((line_count+1))"
    for env_var in "${env_vars[@]}"; do
      var_name="${env_var%%\=*}"
      regex=".*(\\\$${var_name}|\\\$\{$var_name).*"
      if [[ $line =~ $regex ]]; then
        match="${BASH_REMATCH[0]}"
        for exclude in "${excludes[@]}"; do
          if [[ "$match" =~ $exclude ]]; then
            match=""
          fi;
        done;
        if [[ "${match}" != "" ]]; then
          echo "  $line_count $match" 1>&2
          failures="true"
        fi
      fi;
    done;
  done < "${file}"
  if [[ "$failures" == "true" ]]; then
    echo "  One or more variable names in the file ($file) are not authorized; see above." 1>&2
    echo "  Variables names must be (or start with) one of the following:" 1>&2
    for exclude in "${excludes[@]}"; do
      echo "        $exclude"
    done
    return 1
  else
    echo "  No matches! File (${file}) adheres to namespacing standards. :-)"
  fi

}



test_entrypoint_script() {

  local expected_rc
  local script
  local ci_job_name

  script="${1}"
  expected_rc="${2}"
  bash_bin="${3}"
  ci_job_name="${CI_JOB_NAME}"

  echo -e "\n[INFO] Making directory to store kcov coverage report as CI job artifact."
  mkdir -p ".gitlab-ci/artifacts/${ci_job_name}/kcov/" || return 1
  echo -e "\n[INFO] Executing entrypoint script (${script}) using kcov to run test and generate coverage report."
  export PYTHONUNBUFFERED="true"
  {
    kcov \
      --bash-parser="${bash_bin}" \
      --bash-dont-parse-binary-dir \
      --debug-force-bash-stderr \
      ".gitlab-ci/artifacts/${ci_job_name}/kcov/" \
      "${script}"
  } || true

}


main() {

  local pipeline_name
  local pipeline_version
  local entrypoint_script
  local file_to_test
  local bash_bin
  local expected_rc

  pipeline_name="${LOCAL_PIPELINE_NAME}"
  pipeline_version="${LOCAL_PIPELINE_VERSION}"
  entrypoint_script="${LOCAL_ENTRYPOINT_SCRIPT}"
  file_to_test="${pipeline_name}/${pipeline_version}/${entrypoint_script}"
  expected_rc="${LOCAL_RC}"
  bash_bin="${LOCAL_BASH_BIN}"

  check_var_namespaces "${file_to_test}" || return 1
  test_entrypoint_script "${file_to_test}" "${expected_rc}" "${bash_bin}" || return 1

}      

main || exit 1
