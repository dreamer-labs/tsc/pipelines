#!/usr/bin/env bash

login_to_registries() {

  echo -e "\n[INFO] Logging into dependency proxy registry (${CI_DEPENDENCY_PROXY_SERVER})."
  buildah login \
    -u "$CI_DEPENDENCY_PROXY_USER" \
    -p "$CI_DEPENDENCY_PROXY_PASSWORD" \
    "$CI_DEPENDENCY_PROXY_SERVER" || return 1

  echo -e "\n[INFO] Logging into project's container registry (${CI_REGISTRY})."
  buildah login \
    -u "$CI_REGISTRY_USER" \
    -p "$CI_REGISTRY_PASSWORD" \
    "$CI_REGISTRY" || return 1

}

validate_dockerfile() {

  local match
  local mr
  local version
  local failures
  local base
  local check
  local pipeline_name
  local pipeline_version
  local target
  local dockerfile

  failures="false"
  match="false"
  pipeline_name="${LOCAL_PIPELINE_NAME}"
  pipeline_version="${LOCAL_PIPELINE_VERSION}"
  target="${LOCAL_TARGET}"
  dockerfile="${LOCAL_DOCKERFILE}"

  while read -r line; do
          if [[ "$line" =~ FROM.*registry.gitlab.com/dreamer-labs/tsc/pipelines/.*:v.*-mr.*as.*(${target//base/final}|${target//final/tester}) ]]; then
      match="true"
      base="${line##*/}"
      base="${base%%\:*}"
      mr="${line##*-mr}"
      mr="${mr%% as*}"
      version="${line##*\:v}"
      version="${version%%-mr*}"
      echo -e "\n[INFO] Checking if base image with this version (${pipeline_version}) and MR (${CI_MERGE_REQUEST_IID}) exists."
      check="$(
        buildah manifest inspect "${CI_REGISTRY_IMAGE}/${base}:v${version}-mr${mr}" 1>&2
        echo "$?"
      )"
      if [[ "$mr" != "${CI_MERGE_REQUEST_IID}" ]] && [[ "$check" != "0" ]]; then
        echo -e "\n[WARN] Dockerfile lists image with old version of dependent image (mr${mr})." 1>&2
        echo -e "       Should match the MR number of current MR (${CI_MERGE_REQUEST_IID}) since image for MR exists." 1>&2
        echo -e "       Offending line: ${line}" 1>&2
      fi
      if [[ "$version" != "${pipeline_version}" ]]; then
        echo -e "\n[FAIL] Dockerfile lists image with old version of dependent image (${version})." 1>&2
        echo -e "       Should match the version of the pipeline specified in CI job ($pipeline_version)." 1>&2
        echo -e "       Offending line: ${line}" 1>&2
        failures="true"
      fi
    fi
  done <"${pipeline_name}/${pipeline_version}/${dockerfile}"
  if [[ "$match" == "false" ]]; then
    echo -e "\n[FAIL] Dockerfile does not import a properly tagged base image." 1>&2
    failures="true"
  fi

  match="false"
  version=""

  while read -r line; do
    if [[ "$line" =~ COPY[[:blank:]]${pipeline_name}/.* ]]; then
      match="true"
      version="${line//COPY[[:blank:]]"$pipeline_name"\//}"
      version="${version%%\/*}"
      if [[ "$version" != "${pipeline_version}" ]]; then
        echo -e "\n[FAIL] Dockerfile COPYs non-current version of pipeline directory (${version})." 1>&2
        echo -e "       Should match the version of the pipeline specified in CI job ($pipeline_version)." 1>&2
        echo -e "       Offending line: ${line}" 1>&2
        failures="true"
      fi
    fi
  done <"${pipeline_name}/${pipeline_version}/${dockerfile}"
  if [[ "$match" == "false" ]]; then
    echo -e "\n[FAIL] Dockerfile does not COPY a pipeline directory into the image." 1>&2
    failures="true"
  fi

  if [[ "$failures" == "true" ]]; then
    return 1
  fi
}

build_and_push_image() {

  local target
  local tag
  local pipeline_name
  local pipeline_version
  local dockerfile
  local context

  target="${LOCAL_TARGET}"
  tag="${LOCAL_TAG}"
  pipeline_name="${LOCAL_PIPELINE_NAME}"
  pipeline_version="${LOCAL_PIPELINE_VERSION}"
  dockerfile="${LOCAL_DOCKERFILE}"
  context="${LOCAL_CONTEXT}"

  echo -e "\n[INFO] Creating image manifest (${target})."
  buildah manifest create "$target" || return 1

  echo -e "\n[INFO] Building image (${CI_REGISTRY_IMAGE}/${target}:${tag})."
  buildah build \
    --pull \
    -t "${CI_REGISTRY_IMAGE}/${target}:${tag}" \
    --target "$target" \
    --manifest "$target" \
    --arch amd64 \
    --format docker \
    -f "${pipeline_name}/${pipeline_version}/${dockerfile}" \
    "$context" || return 1

  echo -e "\n[INFO] Pushing image (${CI_REGISTRY_IMAGE}/${target}:${tag})."
  buildah manifest push \
    --all \
    "$target" \
    --format v2s2 \
    "docker://${CI_REGISTRY_IMAGE}/${target}:${tag}" || return 1

}

main() {

  local target

  target="${LOCAL_TARGET}"

  login_to_registries || return 1
  case "${target}" in
  *-final | *-base | *-tester)
    validate_dockerfile || return 1
    build_and_push_image || return 1
    ;;
  *-lib)
    build_and_push_image || return 1
    ;;
  *)
    echo -e "\n[FAIL] Failed to pass a properly named target (${target})." 1>&2
    echo "       Targets must end in '-base', '-final', '-tester', or '-lib' to be considered valid." 1>&2
    return 1
    ;;
  esac

}

main || exit 1
