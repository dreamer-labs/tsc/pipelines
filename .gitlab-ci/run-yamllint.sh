#!/usr/bin/env bash

main() {
  
  fail=false

  for file in *.y{,a}ml .gitlab-ci/*.y{,a}ml */*/*.y{,a}ml; do
    if [[ -f "$file" ]]; then
      echo -e "\n=== ${file} ==="
      { yamllint -d relaxed "$file" && echo "no output"; } || fail="true"
      echo -e "=== ${file} ===\n"
    fi
  done

  if [[ "$fail" == "true" ]]; then
    echo "[FAIL] One or more files failed yamllint." 1>&2
    return 1
  fi

}

main || exit 1
