#!/usr/bin/env bash

main() {

  local -A changed_pipelines
  local -a pipeline_versions &&
    local debug &&
    local changes_detected &&
    local changed_file_list &&
    local pipeline_version_list &&
    if [[ "$PIPELINE_DEBUG" == "true" ]]; then debug="true"; else debug="false"; fi &&
    changes_detected=0 &&
    { changed_file_list="$(git diff --name-only remotes/upstream/master 2>/dev/null || git diff --name-only remotes/origin/master)" || return 1; } &&
    while read -r changed_file; do
      {
        if [[ -d "${changed_file%%/*}" ]] && [[ "${changed_file:0:1}" != "." ]]; then
          {
            { { [[ "$debug" == "true" ]] && echo -e "\n[DEBUG] Adding ${changed_file%%/*}/ to list of changed pipelines."; } || true; } &&
              changed_pipelines["${changed_file%%/*}/"]="valuenotused"
          } || return 1
        fi
      } || return 1
    done <<<"$changed_file_list" &&
    { { [[ "$debug" == "true" ]] && echo -e "\n[DEBUG] Deduplicated list of changed pipelines: ${!changed_pipelines[*]}"; } || true; } &&
    for changed_pipeline in "${!changed_pipelines[@]}"; do
      {
        { pipeline_version_list="$(ls -rvd "${changed_pipeline}"*/)" || return 1; } &&
          { { [[ "$debug" == "true" ]] && echo -e "\n[DEBUG] Comparing most recent pipeline versions for changed pipeline ($changed_pipeline)."; } || true; } &&
          while read -r pipeline_version; do
            pipeline_versions+=("$pipeline_version")
          done <<<"$pipeline_version_list" &&
          { { [[ "$debug" == "true" ]] && echo -e "\n[DEBUG] Comparing ${pipeline_versions[0]} & ${pipeline_versions[1]} for changes."; } || true; } &&
          { git diff --no-index --stat "${pipeline_versions[1]}" "${pipeline_versions[0]}" || changes_detected=1; } &&
          pipeline_versions=()
      } || return 1
    done &&
    if [[ "$changes_detected" == "0" ]]; then
      echo -e "\n[PASS] No code changes detected between old and new versions of each updated pipeline.\n"
      exit 0
    else
      echo -e "\n[FAIL] Code changes detected between old and new versions of at least one updated pipeline.\n" 1>&2
      exit 1
    fi

}

main || {
  echo -e "\n[FAIL] Failed with unhandled error.\n" 1>&2
  exit 1
}
